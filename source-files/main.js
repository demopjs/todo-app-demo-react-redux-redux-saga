$(function() {
  var ACTIVE_CLASSNAME = 'is-active';

  var 
    $carousel     = $('[data-carousel-component]'),
    $navItems     = $carousel.find('[data-carousel-nav]'),
    $itemsWrapper = $carousel.find('[data-carousel-items-wrapper]'),
    $itemsBox     = $carousel.find('[data-carousel-items-box]'),
    $items        = $carousel.find('[data-carousel-item]'),
    $addItem      = $carousel.find('[data-carousel-add]');

  // SWIPE

  $(document).on('click', '[data-carousel-item]', function() {
    console.log(isDragging);
    if(isDragging) return;
    selectCarouselItem($(this));
  });
  $navItems.on('click', function(e) {
    // e.preventDefault();
    var isPrev = $(this).data('carousel-direction') == 'prev' ? true : false;
    slideCarousel(isPrev);
  });
  $addItem.on('click', addItem);

  // select first item on init
  selectCarouselItem($items.first().next());


  // SWIPE
  var boxInitialOffset = null, cursorInitialOffset = null, isDragging = false;
  $(document).on('touchstart.swipeStart', handleSwipeStart);
  $(document).on('touchend.swipeEnd', handleSwipeEnd);
  // $(document).on('touchstart', handleTouchStart);
  // $(document).on('touchmove', handleTouchMove);
  // $(document).on('touchend', handleTouchEnd);

  function handleSwipeStart(e) {
    // e.preventDefault();
    var $this = $(e.target);
    if($this.attr('data-carousel-items-box') != undefined || $this.parents('[data-carousel-items-box]').length) {
      boxInitialOffset = parseInt($itemsBox.css('left')) || 0;
      cursorInitialOffset = e.originalEvent.touches[0].pageX;
      $(document).on('touchmove.swipeMove', handleSwipeMove);
    }
    return;
  }
  function handleSwipeEnd(e) {
    // e.preventDefault();
    $(document).off('.swipeMove');
    if (isDragging) {
      boxInitialOffset = null;
      cursorInitialOffset = null;
      selectItemOnSwipeEnd();
      setTimeout(function() {
        isDragging = false;
      }, 10);
    } 
    return;
  }
  function selectItemOnSwipeEnd() {
    var 
      breakpoint = $itemsWrapper.offset().left + ($itemsWrapper.width() / 2);
    $items.each(function(index, item) {
      var 
        $this = $(this),
        itemRightEdge = $this.offset().left + $this.width();
      if(itemRightEdge > breakpoint) {
        selectCarouselItem($(this));
        return false;
      } 
      else if(index + 1 >= $items.length) {
        selectCarouselItem($items.last());
        return false;
      } 
    });
  }
  function handleSwipeMove(e) {
    // e.preventDefault();
    var cursorOffset = e.originalEvent.touches[0].pageX;
    // in case of an accidental move
    if(Math.abs(cursorOffset - cursorInitialOffset) > 5) {
      isDragging = true;
      var 
        offsetDiff = cursorOffset - cursorInitialOffset,
        boxOffset = boxInitialOffset + offsetDiff;
      $itemsBox.css('left', boxOffset);
    } 
    return;
  }






  // ADD ITEM
  function addItem() {
    var $newItem = $items
      .last().clone().text('Элемент ' + ($items.length + 1))
      .appendTo($itemsBox);
    $items = $itemsBox.children();
    selectCarouselItem($newItem);
  }


  // CAROUSEL NAVIGATION
  function slideCarousel(prev) {
    var 
      $activeItem = $items.filter('.' + ACTIVE_CLASSNAME),
      activeItemIndex = $activeItem.index(),
      totalItems = $items.length;

    if(prev && $activeItem.prev().length) {
      selectCarouselItem($activeItem.prev());
    } 
    else if (!prev && $activeItem.next().length) {
      selectCarouselItem($activeItem.next());
    } 
    else {
      return;
    }
  }


  // GENERAL 
  function selectCarouselItem($item) {
    slideToItem($item);
  }

  function slideToItem($item) {
    var 
      boxWidth = $itemsWrapper.width(),
      boxOffset = $itemsBox.offset().left,
      itemWidth = $item.width(),
      itemOffset = $item.offset().left,
      itemRelativeOffset = boxOffset - itemOffset;

    var animateOffset = (boxWidth / 2) + itemRelativeOffset - (itemWidth / 2);
    $itemsBox.animate({
      left: animateOffset
    }, 300, 'swing', function() {
      $item
        .addClass(ACTIVE_CLASSNAME)
        .siblings().removeClass(ACTIVE_CLASSNAME);
    });
  }

});
