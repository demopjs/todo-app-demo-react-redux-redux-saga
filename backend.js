require('coffee-script/register');

var fs = require('fs');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var app = express();
var flash = require('req-flash');

var _ = require('underscore');

var RandExp = require('randexp');


/// From original React
const logger = require('./server/logger');
const argv = require('minimist')(process.argv.slice(2));
const setup = require('./server/middlewares/frontendMiddleware');
const isDev = process.env.NODE_ENV !== 'production';
const ngrok = (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;
const resolve = require('path').resolve;

const port = argv.port || process.env.PORT || 3000;

setup(app, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
});


var auth = function (req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.send(401);
  };

  return next();
};

app.set('port', (process.env.PORT || 4000));

app.use('/', express.static(path.join(__dirname, 'build')));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// MY OWN PARSER
app.use(function(req, res, next) {
  var p = {};
  _.each([req.query, req.params, req.body], function(row) {
    _.each(row,function(val,key) { p[key] = val === 'false' ? false : val === 'true' ? true :  val === 'undefined' ? undefined : val; });
  });
  req.p = p;

  next();
});

// Additional middleware which will set headers that we need on each request.
app.use(function(req, res, next) {
    // Set permissive CORS header - this allows this server to be used only as
    // an API server in conjunction with something like webpack-dev-server.
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Disable caching so we'll always get the latest comments.
    res.setHeader('Cache-Control', 'no-cache');
    next();
});

// SESSIONS
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/list');

app.use(session({
  secret: '8rnMcED0bnsHFP1oqeaQ',
  maxAge: 360 * 24 * 60 * 60 * 1000,
  cookie: { maxAge: 360 * 24 * 60 * 60 * 1000 },
  store: new MongoStore({ mongooseConnection: mongoose.connection,
                          ttl: 360 * 24 * 60 * 60})
}));

// SESSION USERID AND LISTID SETTERS
// TODO: TEST ORDER OF THIS AND USER AUTH!
app.use(function(req, res, next) {
  if (!req.session.userId) {
    if (req.user)
      req.session.userId = req.user.id;
    else
      req.session.userId = new RandExp(/[23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz]{19}/).gen();
  }

  var listId = undefined;
  if (!req.session.listId) {
    if (req.user)  // AUTHORIZED?
      listId = req.user.listId; // GET FROM USER
    if (!listId) // NOWHERE?! OKAY - LETS CREATE NEW
      listId = new RandExp(/[23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz]{17}/).gen();
    req.session.listId = listId;
  }

  next();
});

// FLASH
app.use(flash());


// AUTO REGISTER MODEL
fs.readdirSync('./models').forEach(function (file) {
  if(file.substr(-3) == '.js' || (file.substr(-7) == '.coffee')) {
    console.log('MODEL:'+file);
    var route = require('./models/' + file);
  }
});

var passport = require('./passport');
// PASSPORT JS
app.use(passport.initialize());
app.use(passport.session());
passport.init(app);

// ERROR HANDLING
app.use(function(err, req, res, next) {
  console.error(err);
  res.status(500).end();
});

// MERGE USER AFTER PASSPORT CONTROL
app.use(function(req, res, next) {
  if (req.session.mergeUser)
  {
    var ListController =  require('./controllers/ListController');
    ListController.serverMerge(req.session.mergeUser, function(err,ok) {
        if (err)
          console.log('Err MERGER: ' + JSON.stringify(req.session.mergeUser) + ' ' + err);
        else {
          console.log('MERGE OK: ' + JSON.stringify(req.session.mergeUser));
          req.session.mergeUser = undefined;
        }
        next();
      });
    return;
  }
  next();
});

// AUTO API

fs.readdirSync('./controllers').forEach(function (file) {
  if(file.substr(-3) == '.js' || (file.substr(-7) == '.coffee')) {

    filename = file.substr(0,file.lastIndexOf('.')).replace('Controller','');
    console.log('FILENAME:'+filename);
    var route = require('./controllers/' + file);
    /*_.forEach(route, function(value, key) {
       console.log('API: ' + '/api/'+filename+'/'+key);
       app.post('/api/'+filename+'/'+key, value);
       app.get('/api/'+filename+'/'+key, value);
      });
     */
    app.get('/api/lists/:listId?', route.find);
    app.get('/api/create', route.create);
    app.get('/api/user', route.user);
    app.get('/api/new_list', route.new_list);
    app.post('/api/new_list', route.new_list);
    app.get('/api/new_task/:listId', route.new_task);
    app.post('/api/new_task/:listId', route.new_task);
    app.get('/api/delete_task/:id', route.delete_task);
    app.post('/api/delete_task/:id', route.delete_task);
    app.get('/api/task/:id', route.change_task);
    app.post('/api/task/:id', route.change_task);
    app.get('/api/list/:listId', route.change_list);
    app.post('/api/list/:listId', route.change_list);
  }
});

/*app.get('/addNew', auth,  function(req, res) {
  var listId = new RandExp(/[23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz]{17}/).gen();
  req.session.listId = listId;
  if (res)
    res.redirect("/"+req.session.listId);
});*/

app.listen(port, function(err) {
  console.log('Server started: http://localhost:' + port + '/');

  if (err) {
    return logger.error(err.message);
  }

  // Connect to ngrok in dev mode
  if (ngrok) {
    ngrok.connect(port, (innerErr, url) => {
      if (innerErr) {
        return logger.error(innerErr);
      }

      logger.appStarted(port, url);
    });
  } else {
    logger.appStarted(port);
  }

});
