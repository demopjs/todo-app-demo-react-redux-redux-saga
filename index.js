const 
  browserSync = require('browser-sync'),
  debounce = require('lodash.debounce'),
  path = require('path');

browserSync.init({
  server: {
    baseDir   : './source-files/',
    directory : true,
    index     : 'index.html',
  },
  host      : '192.168.1.106',
  open      : false,
  notify    : false,
  logLevel  : 'info',
  logPrefix : 'BrowserSync',
});
browserSync.watch(path.join('**/*.@(html|css|js)'))
  .on('change', debounce(browserSync.reload, 500));