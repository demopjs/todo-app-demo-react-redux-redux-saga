/**
 * Passport configuration
 *
 * This is the configuration for your Passport.js setup and where you
 * define the authentication strategies you want your application to employ.
 *
 * I have tested the service with all of the providers listed below - if you
 * come across a provider that for some reason doesn't work, feel free to open
 * an issue on GitHub.
 *
 * Also, authentication scopes can be set through the `scope` property.
 *
 * For more information on the available providers, check out:
 * http://passportjs.org/guide/providers/
 */

module.exports =
 {
/*  local: {
    strategy: require('passport-local').Strategy
  },

  bearer: {
    strategy: require('passport-http-bearer').Strategy
  },

  twitter: {
    name: 'Twitter',
    protocol: 'oauth',
    strategy: require('passport-twitter').Strategy,
    options: {
      consumerKey: 'your-consumer-key',
      consumerSecret: 'your-consumer-secret'
    }
  },*/
   /*
  github: {
    name: 'GitHub',
    protocol: 'oauth2',
    strategy: require('passport-github').Strategy,
    options: {
      clientID: 'your-client-id',
      clientSecret: 'your-client-secret'
    }
  },
    */
  facebook: {
    name: 'Facebook',
    protocol: 'oauth2',
    strategy: require('passport-facebook').Strategy,
    options: {
      clientID: '193484224446859',
      clientSecret: '0ee136b862da788319a50b9e88bca088',
      scope: ['email'], /* email is necessary for login behavior */
      callbackURL: "/auth/facebook/callback",
      //profileFields: ['id','name','first_name','last_name','age_range','link','gender','locale','picture']
      profileFields: ['id', 'displayName', 'photos', 'email']
    }
  },

  vkontakte: {
    name: 'ВКонтакте',
    protocol: 'oauth2',
    strategy: require('passport-vkontakte').Strategy,
    options: {
      clientID:     '5775255', // VK.com docs call it 'API ID', 'app_id', 'api_id', 'client_id' or 'apiId'
      clientSecret: 'THHCltpahm4LlnU3oHBq',
      callbackURL:  "/auth/vkontakte/callback"
    }
  },

  odnoklassniki: {
    name: 'Одноклассники',
    protocol: 'oauth2',
    strategy: require('passport-ok').Strategy,
    options: {
      clientID:     '1249149696',
      clientSecret: 'C623DB7E84A4DC5E372FB1ED',
      clientPublic: 'CBAEIFHLEBABABABA',
      callbackURL:  "/auth/odnoklassniki/callback"
    }
  }

  /*
  google: {
    name: 'Google',
    protocol: 'oauth2',
    strategy: require('passport-google-oauth').OAuth2Strategy,
    options: {
      clientID: 'your-client-id',
      clientSecret: 'your-client-secret'
    }
  },

  cas: {
    name: 'CAS',
    protocol: 'cas',
    strategy: require('passport-cas').Strategy,
    options: {
      ssoBaseURL: 'http://your-cas-url',
      serverBaseURL: 'http://localhost:1337',
      serviceURL: 'http://localhost:1337/auth/cas/callback'
    }
  }
*/
// TODO: vk, mail,ok

 };
