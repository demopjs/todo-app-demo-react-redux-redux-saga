var path     = require('path')
  , url      = require('url')
  , passport = require('passport');

var _ = require('underscore');

var RandExp = require('randexp');


var strategies = require('./config')

var Passport = require('mongoose').model('Passport');
var User = require('mongoose').model('User');

var ListController =  require('../controllers/ListController');

/*
 * For more information on auth(entication|rization) in Passport.js, check out:
 * http://passportjs.org/guide/authenticate/
 * http://passportjs.org/guide/authorize/
 *
 * @param {Object}   req
 * @param {Object}   query
 * @param {Object}   profile
 * @param {Function} next
 */
passport.connect = function (req, query, profile, next) {
  console.log('CONNECT ' + JSON.stringify(query) + ' PROFILE ' + JSON.stringify(profile) );
  var user = {}
    , provider;

  // Get the authentication provider from the query.
  query.provider = req.param('provider');

  // Use profile.provider or fallback to the query.provider if it is undefined
  // as is the case for OpenID, for example
  provider = profile.provider || query.provider;

  // If the provider cannot be identified we cannot match it to a passport so
  // throw an error and let whoever's next in line take care of it.
  if (!provider){
    console.log('ERROR: NO PROVIDER');
    return next(new Error('No authentication provider was identified.'));
  }

  // If the profile object contains a list of emails, grab the first one and
  // add it to the user.
  if (profile.hasOwnProperty('emails')) {
    if (profile.emails.length>0) {
      user.email = profile.emails[0].value;
      console.log('GIVE MAIL: '+ user.email);
    }
  }
  if (profile.hasOwnProperty('photos')) {
    if (profile.photos.length>0) {
      user.photo = profile.photos[0].value;
      console.log('GIVE PHOTO: '+ user.photo);
    }
  }

  // If the profile object contains a username, add it to the user.
  if (profile.hasOwnProperty('displayName')) {
    user.username = profile.displayName;
    console.log('GIVE NAME: '+ profile.displayName);
  }

  // If neither an email or a username was available in the profile, we don't
  // have a way of identifying the user in the future. Throw an error and let
  // whoever's next in the line take care of it.
  if (!user.username && !user.email) {
    console.log('neither NAME AND EMAIL found');
    //return next(new Error('Neither a username nor email was available'));
  }

  console.log('FINDING USER... '+query.identifier.toString());
  Passport.findOne({
    provider   : provider
  , identifier : query.identifier.toString()
  }, function (err, passport) {
    if (err) {
      console.log('ERROR: found in DB user');
      return next(err);
    }

    if (!req.user) {
      // Scenario: A new user is attempting to sign up using a third-party
      //           authentication provider.
      // Action:   Create a new user and assign them a passport.
      console.log('NO USER NOW');
      if (!passport) {
        console.log('NO PASSPORT NOW');

        var myPassId = new RandExp(/[23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz]{20}/).gen();
        var pass = [];
        pass.push(myPassId);
        user.passports = pass;
        if (req.session.userId)
          user._id = req.session.userId;
        else
          user._id = new RandExp(/[23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz]{19}/).gen();

        new User(user).save(function (err) {
          if (err) {
            if (err.code === 'E_VALIDATION') {
              if (err.invalidAttributes.email) {
                console.log('Error.Passport.Email.Exists');
                req.flash('error', 'Error.Passport.Email.Exists');
              }
              else {
                console.log('Error.Passport.User.Exists');
                req.flash('error', 'Error.Passport.User.Exists');
              }
            }

            return next(err);
          }
          console.log('USER CREATED: '+user._id);

          query.user = user._id;
          query._id = myPassId;
          if (profile) {
            query.profile = profile;
          }

          console.log('NOW PASSPORT CREATE '+JSON.stringify(query));
          new Passport(query).save(function (err) {
            console.log('PASSPORT CREATE: '+JSON.stringify(err));
            // If a passport wasn't created, bail out
            if (err) {
              console.log('ERROR: PASSPORT CREATE '+JSON.stringify(query));
              return next(err);
            }

            next(err, user);
          });
        });
      }
      // Scenario: An existing user is trying to log in using an already
      //           connected passport.
      // Action:   Get the user associated with the passport.
      else {
        console.log('ALREADY PASSPORT');
        // If the tokens have changed since the last session, update them
        if (query.hasOwnProperty('tokens') && query.tokens !== passport.tokens) {
          passport.tokens = query.tokens;
        }
        if (profile) {
          passport.profile = profile;
        }

        // Save any updates to the Passport before moving on
        passport.save(function (err) {
          if (err) {
            return next(err);
          }
          console.log('ALREADY PASSPORT GET USER '+JSON.stringify(passport));
          // Fetch the user associated with the Passport
          User.findById(passport.user, function(err,user) {
            if (!err && user && user._id) {
              if (req.session.userId)
                req.session.mergeUser = { oldUserId: req.session.userId, newUserId: user._id };
              req.session.userId = user._id;
            }
            next(err,user);
          });
        });
      }
    } else {
      console.log('USER LOGINED ALREADY');
      if (req.user._id != req.session.userId)
      {
        if (req.session.userId)
          req.session.mergeUser = { oldUserId: req.session.userId, newUserId: req.user._id };
        req.session.userId = req.user._id;

      }
      // Scenario: A user is currently logged in and trying to connect a new
      //           passport.
      // Action:   Create and assign a new passport to the user.

      // TODO: UPDATE USER INFO

      if (!passport) {
        query.user = req.user._id;
        var myPassId = new RandExp(/[23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz]{20}/).gen();
        query._id = myPassId;
        console.log('USER LOGINED ALREADY NO PASSPORT: '+ JSON.stringify(query));
        if (profile) {
          query.profile = profile;
        }

        req.user.passports.push(myPassId);
        req.user.markModified('passports');
        req.user.save();

        new Passport(query).save(function (err) {
          // If a passport wasn't created, bail out
          if (err) {
            return next(err);
          }
          console.log('PASSPORT CREATED');
          next(err, req.user);
        });
      }
      // Scenario: The user is a nutjob or spammed the back-button.
      // Action:   Simply pass along the already established session.
      else {
        console.log('USER LOGINED ALREADY, AND PASSPORT OK');
        next(null, req.user);
      }
    }
  });
};




/*
 * Create an authentication endpoint
 *
 * For more information on authentication in Passport.js, check out:
 * http://passportjs.org/guide/authenticate/
 *
 * @param  {Object} req
 * @param  {Object} res
 */
passport.endpoint = function (req, res) {
    var provider   = req.param('provider')
    , options    = {};

  // If a provider doesn't exist for this endpoint, send the user back to the
  // login page
  if (!strategies.hasOwnProperty(provider)) {
    return res.redirect('/login');
  }

  // Attach scope if it has been set in the config
  if (strategies[provider].hasOwnProperty('scope')) {
    options.scope = strategies[provider].scope;
  }

  // Redirect the user to the provider for authentication. When complete,
  // the provider will redirect the user back to the application at
  //     /auth/:provider/callback
  console.log('AUTH 1');
  this.authenticate(provider, options)(req, res, req.next);
};

/**
 * Create an authentication callback endpoint
 *
 * For more information on authentication in Passport.js, check out:
 * http://passportjs.org/guide/authenticate/
 *
 * @param {Object}   req
 * @param {Object}   res
 * @param {Function} next
 */
passport.callback = function (req, res, next) {
  var protocols = require('./protocols');
  var provider = req.param('provider', 'local')
    , action   = req.param('action');
  console.log('CALLBACK '+provider+' '+action);
  // Passport.js wasn't really built for local user registration, but it's nice
  // having it tied into everything else.
  if (provider === 'local' && action !== undefined) {
    if (action === 'register' && !req.user) {
      protocols.local.register(req, res, next);
    }
    else if (action === 'connect' && req.user) {
      protocols.local.connect(req, res, next);
    }
    else if (action === 'disconnect' && req.user) {
      this.disconnect(req, res, next);
    }
    else {
      next(new Error('Invalid action'));
    }
  } else {
    if (action === 'disconnect' && req.user) {
      this.disconnect(req, res, next) ;
    } else {
      // The provider will redirect the user to this URL after approval. Finish
      // the authentication process by attempting to obtain an access token. If
      // access was granted, the user will be logged in. Otherwise, authentication
      // has failed.
      console.log('AUTH 2');
      this.authenticate(provider, next)(req, res, req.next);
    }
  }
};

/**
 * Load all strategies defined in the Passport configuration
 *
 * For example, we could add this to our config to use the GitHub strategy
 * with permission to access a users email address (even if it's marked as
 * private) as well as permission to add and update a user's Gists:
 *
    github: {
      name: 'GitHub',
      protocol: 'oauth2',
      strategy: require('passport-github').Strategy
      scope: [ 'user', 'gist' ]
      options: {
        clientID: 'CLIENT_ID',
        clientSecret: 'CLIENT_SECRET'
      }
    }
 *
 * For more information on the providers supported by Passport.js, check out:
 * http://passportjs.org/guide/providers/
 *
 */
passport.loadStrategies = function () {
  var self       = this;
  var protocols = require('./protocols')(this);
  Object.keys(strategies).forEach(function (key) {
    var options = { passReqToCallback: true }, Strategy;

    if (key === 'local') {
      // Since we need to allow users to login using both usernames as well as
      // emails, we'll set the username field to something more generic.
      _.extend(options, { usernameField: 'identifier' });

      //Let users override the username and passwordField from the options
      _.extend(options, strategies[key].options || {});

      // Only load the local strategy if it's enabled in the config
      if (strategies.local) {
        Strategy = strategies[key].strategy;

        self.use(new Strategy(options, protocols.local.login));
      }
    } else if (key === 'bearer') {

      if (strategies.bearer) {
        Strategy = strategies[key].strategy;
        self.use(new Strategy(protocols.bearer.authorize));
      }

    } else {
      var protocol = strategies[key].protocol
        , callback = strategies[key].callback;

      if (!callback) {
        callback = 'auth/' + key + '/callback';
      }

      Strategy = strategies[key].strategy;
      var basePort = process.env.PORT || 3000;
      // var baseUrl = 'https://take-job.ru/';
      var baseUrl = `http://localhost:${basePort}/`;
      // var baseUrl = 'https://take-job.ru/';

      _.extend(options, strategies[key].options);

      switch (protocol) {
        case 'oauth':
        case 'oauth2':
          options.callbackURL = baseUrl + callback;
          break;

        case 'openid':
          options.returnURL = baseUrl + callback;
          options.realm     = baseUrl;
          options.profile   = true;
          break;
      }

      // Merge the default options with any options defined in the config. All
      // defaults can be overriden, but I don't see a reason why you'd want to
      // do that.
      console.log(JSON.stringify({ load: "strategy", opt: options, protName: protocol }));
      console.log(protocols[protocol]);
      self.use(new Strategy(options, protocols[protocol]));
    }
  });
};

/**
 * Disconnect a passport from a user
 *
 * @param  {Object} req
 * @param  {Object} res
 */
passport.disconnect = function (req, res, next) {
  var user     = req.user
    , provider = req.param('provider', 'local')
    , query    = {};

  query.user = user._id;
  query[provider === 'local' ? 'protocol' : 'provider'] = provider;

  Passport.findOne(query, function (err, passport) {
    if (err) {
      return next(err);
    }

    Passport.destroy(passport._id, function (error) {
      if (err) {
          return next(err);
      }

      next(null, user);
    });
  });
};

passport.serializeUser(function (user, next) {
  next(null, user._id);
});

passport.deserializeUser(function (id, next) {
  User.findById(id, next);
});


passport.init = function (app)
{
  this.loadStrategies();
  app.get('/auth/:provider', function(req, res) {
    if (req.p['callback']) {
      req.session.callback = req.p['callback'];
      console.log('CALBACK URL ======================== '+req.session.callback);
      if (req.p['retUrl']) {
        req.session.retUrl = decodeURI(req.p['retUrl']);
        console.log('RETURN URL ======================== ' + req.session.retUrl);
      }
    }
    else {
      req.session.callback = undefined;

      if (req.p['retUrl']) {
        req.session.retUrl = decodeURI(req.p['retUrl']);
        console.log('RETURN URL ======================== ' + req.session.retUrl);
        req.session.retAction = req.p['action'];
        try {
          req.session.retOptions = JSON.parse(decodeURI(req.p['options']));
        } catch (er) {
          req.session.retUrl = undefined;
          req.session.retAction = undefined;
          req.session.retOptions = undefined;
        }
      }
      else {
        req.session.retUrl = undefined;
        req.session.retAction = undefined;
        req.session.retOptions = undefined;
      }
    }
    passport.endpoint(req, res);
  });
  app.get('/auth/:provider/callback', function(req, res) {
    console.log('CALLBACK RUN');
    passport.callback(req, res, function (err, user, challenges, statuses) {
      console.log('CALLBACK:'+JSON.stringify({ er: err, us: user, ch: challenges, s: statuses }));
      if (err || !user) {
        return; //tryAgain(challenges);
      }

      req.login(user, function (err) {
        if (err) {
          return; //tryAgain(err);
        }
        req.session.authenticated = true;
        if (req.session.callback) {
          console.log('CALBACK URL -------------- '+req.session.callback);
          console.log('RETURN URL ------------- ' + req.session.retUrl);
          var path = (req.session.retUrl ? encodeURI(req.session.retUrl) : '' )+ '/?callback=' + req.session.callback;
          console.log('RETURN PATH '+ path );
          res.redirect(path);
          req.session.callback = undefined;
          req.session.retUrl = undefined;
        }
        else {
          if (req.session.retAction) {
            console.log('DO ACTION ' + req.session.retAction, req.session.retOptions);
            //TODO
            req.session.retAction = undefined;
          }
          res.redirect(req.session.retUrl || '/');
          req.session.retUrl = undefined;
        }
      });
    });
  });
}


module.exports = passport;
