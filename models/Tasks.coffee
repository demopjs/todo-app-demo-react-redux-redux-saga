mongoose = require('mongoose')
timestamps = require('mongoose-timestamp')
ShortId = require('mongoose-shortid-nodeps')
Schema = mongoose.Schema

taskSchema = new Schema({
  _id:
    type: ShortId
    len: 20
    alphabet: '23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz'
  completed: Boolean
  cId: String
  title: String
  description: String
  listId:
    type: String
    index: true
  authorId: String
}, collection: 'task')

taskSchema.plugin(timestamps);

taskSchema.query.byListId = (listId) ->
  @find(
    listId: listId
  ).sort('createdAt')

taskSchema.query.byListIds = (listIds) ->
  @find(
    listId:
      $in:
        listIds
  ).sort('createdAt')

task = mongoose.model('Tasks', taskSchema)
module.exports = task
