mongoose = require('mongoose')
ShortId = require('mongoose-shortid-nodeps')
Schema = mongoose.Schema

schemaUser = new Schema({
    _id:
      type: ShortId
      len: 19
    username: String,
    email: String,
    photo: String,
    passports: [{
      type: String
    }]
  })

User = mongoose.model('User', schemaUser)
module.exports = User
