bcrypt = require('bcryptjs')
mongoose = require('mongoose')
ShortId = require('mongoose-shortid-nodeps')
Schema = mongoose.Schema

schemaPassport = new Schema({
    _id:
      type: ShortId
      len: 20
    protocol:
      type: String
      required: true
    password:
      type: String
      minLength: 8
    accessToken:
      type: String
    provider:
      type: String
    identifier:
      type: String
    tokens:
      type: Schema.Types.Mixed
    profile:
      type: Schema.Types.Mixed
    user:
      type: String
      required: true
  })
###
# Hash a passport password.
#
# @param {Object}   passport
# @param {Function} next
###

hashPassword = (passport, next) ->
  if passport.password
    bcrypt.hash passport.password, 10, (err, hash) ->
      passport.password = hash
      next err, passport
  else
    next null, passport

schemaPassport.methods.validatePassword = (passport, next) ->
  bcrypt.compare password, @password, next

schemaPassport.pre 'save', (next) ->
  hashPassword @, next

Passport = mongoose.model('Passport', schemaPassport)
module.exports = Passport
