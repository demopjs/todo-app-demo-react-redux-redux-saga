mongoose = require('mongoose')
timestamps = require('mongoose-timestamp')
ShortId = require('mongoose-shortid-nodeps')
Schema = mongoose.Schema

listSchema = new Schema({
  _id:
    type: String
    len: 19
  completed: Boolean
  deleted: Boolean
  title: String
  authorId: String
}, collection: 'list')

listSchema.plugin(timestamps);

List = mongoose.model('List', listSchema)
module.exports = List
