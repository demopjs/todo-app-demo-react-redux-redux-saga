var _ = require('underscore');

var Tasks = require('mongoose').model('Tasks');

var List = require('mongoose').model('List');
var User = require('mongoose').model('User');
var Passport = require('mongoose').model('Passport');
var mongoose = require('mongoose');

var passportConfig = require('../passport/config');

var RandExp = require('randexp');

function setMe(obj, userId) {
//  console.log('setMe ' + JSON.stringify(obj) + ' '+userId);
  obj.owned = false;
  obj.ownername = '';
  obj.ownerSN  = 'vkontakte';
  obj.ownerUrl = 'https://vk.com/sabitov_k';

  if (obj.completed) {
    //obj.status = 'checked';
    if (obj.cId)
      if (userId)
        if (obj.cId === userId)
          obj.owned = true;
        else {
          obj.owned = false;
          obj.ownername = 'ownername'; // TODO
        }
  }
  obj.name = obj.title;
//  console.log('setMe COMPLETED ' + JSON.stringify(obj) + ' '+userId);
  return _.pick(obj, ['id', 'completed', 'name', 'description', 'owned', 'ownername', 'ownerSN', 'ownerUrl']);
}

function createMainList(listId, authorId, selfnext, next) {

  //console.log('createMainList '+JSON.stringify(listId));
  List.findById(listId, function (err, list) {
    if (err)
      return selfnext({ code: 101, message: err });
    if (list === null || list === undefined) {
      //console.log('createMainList FIND BY AUTHOR '+JSON.stringify(authorId));
      List.find({authorId: authorId}, function (err, list) {
        var number = 1;
        if (err)
          return selfnext({ code: 102, message: err });

        if (_.findWhere(list, {title: ''}))
          return selfnext({ code: 103, message: 'Безымянный список уже существует. Заполните его или переименуйте.' });
        List.create({_id: listId, authorId: authorId, title: ''}, function (err, list) {
          if (err)
            return selfnext({ code: 104, message: err });
          //console.log('createMainList CREATE OK'+JSON.stringify(list));
          return next(); // окей - создали список
        });
      });
    }
    else
      return next(); // список есть, идём дальше
  });

}

function testListId(req, selfnext, next) {
  var listId = req.p['listId'];
  if (!listId)
    return next(req.session.listId);
  List.findById(listId, function (err, list) {
    if (err)
      return selfnext({ code: 201, message: err });
    if (list === null || list === undefined)
      return next(req.session.listId);
    if (list.authorId !== req.session.userId)
      return next(req.session.listId);
    return next(listId);
  });
}

function serverUpdateFunc(req, params, next) {
  Tasks.findById(params.id, function (err, tasks) {
    if (tasks === undefined) return next('NOT FOUND');
    if (tasks === null) return next('NOT FOUND');
    if (err) return next(err);
    var whMaster = (tasks.authorId === req.session.userId);
    if (params.hasOwnProperty('completed')) {
      // снятие только своей галки
      if (tasks.completed && (!(params.completed)) && tasks.cId != req.session.userId)
        return next('NOT AUTHORIZED 1');
      // установка неустановленной галки
      if (!tasks.completed && params.completed)
        params.cId = req.session.userId;
    }
    if (params.hasOwnProperty('title')) {
      // изменение позиции не в своём листе
      if (!whMaster && tasks.title != params.title)
        return next('NOT AUTHORIZED 2');
      // изменение помеченной позиции
      if (tasks.completed && tasks.title != params.title)
        return next('NOT AUTHORIZED 3');
    }
    _.extend(tasks, params);
    tasks.save(function (err) {
      if (err) return next(err);
      var newtask = tasks;
      next(null,setMe(newtask, req.session.userId));
    });
  });
}

function prepareList(list, req, next)
{
  var arrListId = _.pluck(list, '_id');
  Tasks.find().byListIds(arrListId).exec(function (err, tasks) {
    console.log('LIST IDS COMPLETED = ' + JSON.stringify(tasks)+ ' '+err);
    if (err) return next(err);
    if (tasks === undefined) tasks = [];
    if (tasks === null) tasks = [];
    var wl = {};
    _.each(list, function (o) {
      var n = { id: o._id, name: o.title, owned: o.authorId === req.session.userId , tasks: [], ownername: 'ownerLISTname', ownerUrl: 'https://vk.com/id1', ownerSN: 'vkontakte' };
      wl[o._id] = n;
    });
    _.each(tasks, function (o) {
      wl[o.listId].tasks.push(setMe(o, req.session.userId));
    });

    return next(null, _.toArray(wl));
  });
}

function getList(req, getall, next) { /* ДУБЛИРОВАННЫЙ КОД. Как от этого избавляются другие проекты? */
  createMainList(req.session.listId, req.session.userId, next, function () {
    var listId = req.params.listId;
    if (!listId)
      listId = req.session.listId;

    List.findById(listId, function(err, ownlist) {
      if (err) return next({ code: 0, message: err }, []);
      if (ownlist === undefined) return next({ code: 1, message: 'NOT FOUND '+listId}, []);
      if (ownlist === null) return next({ code: 2, message: 'NOT FOUND '+listId}, []);
      console.log('getList getall='+getall+' '+ownlist.userId+' = '+req.session.userId);
      if (!getall /*|| ownlist.authorId !== req.session.userId*/) {
        var arr = [];
        arr.push(ownlist);
        return prepareList(arr, req, next);
      }
      else {
        List.find({authorId: req.session.userId }).exec(function (err, list) {
          console.log('list.find '+JSON.stringify(list));
          if (err) return next({ code: 3, message: err }, []);
          if (list === undefined) return next({ code: 4, message: 'NOT FOUND BY AUTHOR'}, []);
          if (list === null) return next({ code: 5, message: 'NOT FOUND BY AUTHOR'}, []);
          if (ownlist.authorId !== req.session.userId) {
            list.push(ownlist);
          }
          return prepareList(list,req,next);
        });
      }
    });

  });
}


function serverFindUser(req, next) {
  function convertUserToArray(err, passports) {
    if (err) return next(err);
    //console.log('serverUsersFind AFTER Passport FIND '+JSON.stringify(passports));
    var userItems = [];
    //console.log('serverUsersFind BEFORE passportConfig '+JSON.stringify(Object.keys(passportConfig)));
    _.each(passportConfig, function (socNetVal, socNetKey) {
      //console.log('SOCIAL FIND: ' + socNetKey + ' '+JSON.stringify(socNetVal));
      var fPassport = _.findWhere(passports, {provider: socNetKey});
      //console.log('FIND: ' + JSON.stringify(fPassport));
      var profile = undefined;
      if (fPassport) {
        profile = fPassport.profile;
      }
      var userItem = {name: socNetKey, title: socNetVal.name, id: socNetKey};
      //console.log('USERITEM: BEFORE ' + JSON.stringify(userItem));
      if (profile) {
        userItem.id = fPassport._id;
        userItem.logined = true;
        userItem.username = profile.displayName;
        if (profile.photos && profile.photos.length > 0)
          userItem.pic = profile.photos[0].value;
      }
      //console.log('USERITEM: AFTER ' + JSON.stringify(userItem));
      userItems.push(userItem);
    });
    //console.log('serverUsersFind AFTER EACH '+JSON.stringify(userItems));
    return next(null, userItems);
  }

  var userId = req.session.userId;
  if (!userId)
    next(null, []);
  else {
    //console.log('serverUsersFind BEFORE USER FIND '+userId);
    User.findById(userId).exec(function (err, user) {
      if (err) return next(err);
      if (!user)
        return convertUserToArray(null, []);
      //console.log('serverUsersFind BEFORE Passport FIND '+JSON.stringify(user.passports));
      Passport.find({_id: {$in: user.passports}}).exec(convertUserToArray);
    });
  }
}

module.exports = {
  create: function (req, res, next) {

    testListId(req,next,function(listId) {
      var params = {};
      _.each(['id', 'completed', 'title'], function (v) {
        params[v] = req.p[v]
      });

      params.listId = listId;
      params.authorId = req.session.userId;
      if (params.completed)
        params.cId = req.session.userId;

      createMainList(params.listId, req.session.userId, next, function () {
        Tasks.create(params, function (err, tasks) {
          //console.log('CONTROLLER CREATE COMPLETED ' + JSON.stringify(tasks) + ' ' + JSON.stringify(err));
          if (err) return next(err);
          res.status(200);
          tasks = setMe(tasks, req.session.userId);
          res.json(tasks);
        });
      });
    });
  },
  serverMerge: function (obj, next) {
    //req.session.mergeUser = { oldUserId: req.session.userId, newUserId: req.user._id };
    List.update({ authorId: obj.oldUserId }, { $set: { authorId: obj.newUserId} }, { multi: true }).exec(function (err) {
      if (err)
        next(err);
      else
        Tasks.update({ authorId: obj.oldUserId }, { $set: { authorId: obj.newUserId} }, { multi: true }).exec(function (err) {
            next(err);
        });
    });

  },
  //serverFind: getList(req,next),
  user: function (req, res, next)
  {
    serverFindUser(req, function (err, passports) {
      if (err)
        return res.status(400).json(err);
      else {
        var user = {};
        var firstLoginedPass = _.find(passports, { 'logined': true });
        user.logined = !!firstLoginedPass;
        if (firstLoginedPass) {
          user.username = firstLoginedPass.username;
        }
        user.passports = passports;
        return res.json(user);
      }
    });
  },
  find: function (req, res, next) {
    getList(req, true, function(err, wl) {
      //console.log('GET LIST INSIDE '+err+'  wl=' + JSON.stringify(wl));
      if (err)
        return res.status(400).json(err);
      res.json(wl);
    });

  },
  new_list: function (req, res, next) {
    listId = new RandExp(/[23456789ABCDEFGHJKLMNPQRSTWXYZabcdefghijkmnopqrstuvwxyz]{17}/).gen();
    req.session.listId = listId;

    getList(req, false, function(err, wl) {
      console.log('GET NEW LIST INSIDE '+err+'  wl=' + JSON.stringify(wl));
      if (err)
        return res.status(400).json(err);
      if (!wl)
        return res.status(400).json({ code: 99, message: 'UNKNOWN ERROR'});
      return res.json(wl[0]);
    });

  },
  new_task: function (req, res, next) {
    var listId = req.params.listId;
    if (!listId)
      return res.status(400).json({ code: 6, message: 'LIST ID REQUIRED'});

    List.findById(listId, function(err, ownlist) {
      if (err) return res.status(400).json({ code: 0, message: err });
      if (ownlist === undefined) return res.status(400).json({ code: 1, message: 'LIST NOT FOUND '+listId});
      if (ownlist === null) return res.status(400).json({ code: 2, message: 'LIST NOT FOUND '+listId});
      if (ownlist.authorId !== req.session.userId) {
        res.status(400).json({ code: 3, message: 'NOT AUTH FOR LIST '+listId});
      }
      var params = {};
      params['title'] = req.p['name'];
      params['description'] = req.p['description'];
      params['completed'] = req.p['completed'];
      if (params['completed'])
        params['cId'] = req.session.userId;
      params['listId'] = listId;
      params['authorId'] = req.session.userId;
      Tasks.create(params, function (err, task) {
        if (err) return res.status(400).json({ code: 4, message: err });
        res.status(200);
        res.json(setMe(task, req.session.userId));
      });
    });
  },
  delete_task: function (req, res, next) {
    var id = req.params.id;
    Tasks.findById(id, function (err, task) {
      if (err)
        return res.status(400).json({ code: 0, message: err });
      if (!task)
        return res.status(400).json({ code: 1, message: 'Не найдено' });
      if (task.authorId !== req.session.userId)
        return res.status(400).json({ code: 3, message: 'Нельзя удалить не Ваше' });
      if (task.completed && task.cId !== req.session.userId)
        return res.status(400).json({ code: 4, message: 'Нельзя удалить помеченную не Вами' });
      task.remove(
        function (err) {
          if (err) return res.status(400).json({ code: 5, message: err });
          res.json({success: true, id: id});
        });
    });
  },
  change_task: function (req, res, next) {
    var id = req.params.id;
    Tasks.findById(id, function (err, task) {
      if (err)
        return res.status(400).json({ code: 0, message: err });
      if (!task)
        return res.status(400).json({ code: 1, message: 'Не найдено' });
      //if (task.authorId !== req.session.userId)
      //  return res.status(400).json({ code: 3, message: 'Нельзя изменить не Ваше' });
      //if (task.completed)
      //  return res.status(400).json({ code: 4, message: 'Нельзя изменить помеченную' });

      var params = {};
      if (req.p.hasOwnProperty('name'))
        params['title'] = req.p['name'];
      if (req.p.hasOwnProperty('description'))
        params['description'] = req.p['description'];
      if (req.p.hasOwnProperty('completed'))
        params['completed'] = req.p['completed'];

      if (params.hasOwnProperty('completed')) {
        // снятие только своей галки
        if (task.completed && (!(params.completed)) && task.cId != req.session.userId)
          return res.status(400).json({ code: 5, message: 'Снять можно только свою галку' });
        // установка неустановленной галки
        if (!task.completed && params.completed)
          params.cId = req.session.userId;
      }
      if (params.hasOwnProperty('title')) {
        // изменение позиции не в своём листе
        if ((task.authorId !== req.session.userId) && (task.title != params.title)) {
          return res.status(400).json({code: 6, message: 'Менять можно только своё'});
        }
        // изменение помеченной позиции
        if (task.completed && task.title != params.title)
          return res.status(400).json({ code: 7, message: 'Нельзя менять уже помеченную позицию' });
      }
      if (params.hasOwnProperty('description')) {
        // изменение позиции не в своём листе
        if ((task.authorId !== req.session.userId) && (task.title != params.title)) {
          return res.status(400).json({code: 8, message: 'Менять можно только своё'});
        }
        // изменение помеченной позиции
        if (task.completed && task.title != params.title)
          return res.status(400).json({ code: 9, message: 'Нельзя менять уже помеченную позицию' });
      }
      _.extend(task, params);
      task.save(function (err) {
        if (err) return res.status(400).json({ code: 10, message: err });
        return res.status(200).json(setMe(task, req.session.userId));
      });
     });
  },

  change_list: function (req, res, next) {
    console.log('HERE');
    var listId = req.params.listId;
    if (!listId)
      return res.status(400).json({ code: 6, message: 'LIST ID REQUIRED'});

    List.findById(listId, function(err, ownlist) {
      if (err) return res.status(400).json({code: 0, message: err});
      if (ownlist === undefined) return res.status(400).json({code: 1, message: 'LIST NOT FOUND ' + listId});
      if (ownlist === null) return res.status(400).json({code: 2, message: 'LIST NOT FOUND ' + listId});
      if (ownlist.authorId !== req.session.userId) {
        res.status(400).json({code: 3, message: 'Менять можно только своё'});
      }
      var params = {};
      if (req.p.hasOwnProperty('name'))
        ownlist.title = req.p['name'];
      ownlist.save(function (err) {
        if (err) return res.status(400).json({ code: 10, message: err });
        var n = { id: ownlist._id, name: ownlist.title, owned: ownlist.authorId === req.session.userId, ownername: 'ownerLISTname', ownerUrl: 'https://vk.com/id1', ownerSN: 'vkontakte' };

        return res.status(200).json(n);
      });

    });

  },
  changeTitle: function (req, res, next) {
    var id = req.p['listId'];
    var title = req.p['title'];
    console.log('changeTitle: listId='+id+' title='+title);
    var listId = req.session.listId;
    console.log('changeTitle: listId='+id+'='+listId+' title='+title);
    if (id !== listId)
      return next('NOT AUTH');
    // WTF!!!
    List.findById(listId, function (err, task) {
      console.log('changeTitle: FIND LIST '+err+' '+JSON.stringify(task));
      if (err) return next(err);
      task.title = title;
      task.save(function (err) {
        console.log('changeTitle: SAVE '+err);
        if (err) return next(err);
        res.json({ title: title });
      });
    });
  }
};

