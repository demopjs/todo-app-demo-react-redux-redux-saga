import * as types from 'constants/general-action-types';

import { encodeCallback } from 'modules/callback-utils';

// export const fetchAllLists = () => {
//   return {
//     type    : types.FETCH_ALL_LIST,
//     payload : listsMock,
//   };
// };

export const fetchLists = (id) => {
  return {
    type : types.FETCH_LISTS,
    id,
  };
};

/// LISTS
export const addList = (data) => {
  return {
    type : types.ADD_LIST_REQUEST,
    data,
  };
};
export const updateList = (data) => ({
  type : types.UPDATE_LIST_REQUEST,
  data,
});

/// TASKS
export const addTask = (listId, data) => ({
  type : types.ADD_TASK_REQUEST,
  listId,
  data,
});
export const updateTask = (id, data) => {
  return {
    type : types.UPDATE_TASK_REQUEST,
    id, 
    data,
  };
};
export const updateTaskIndex = (id, data) => {
  return {
    type : types.UPDATE_TASK_INDEX_REQUEST,
    id, 
    data,
  };
};

export const deleteTask = (listId, id) => {
  return {
    type : types.DELETE_TASK_REQUEST,
    listId, 
    id,
  };
};




export const fetchUser = () => {
  return {
    type: types.FETCH_USER,
  };
};

export const userLogin = (service, callback) => {
  let domain = '';
  let callbackString = '';
  if(NODE_ENV !== 'production') domain = 'retUrl=http%3A//localhost:3000&';

  if(callback) {
    try { 
      const callbackEncoded = encodeCallback(callback);
      if(callbackEncoded) {
        callbackString = 'callback=' + callbackEncoded;
      }
    }
    catch(e) {
      console.error('Error processing callback in userLogin api action', e);
    }
  }

  const loginUrl = `/auth/${service}?${domain}${callbackString}`;
  window.location.replace(loginUrl);
  // return {
  //   type : types.USER_LOGIN,
  // };
};

export const userLogout = () => ({
  type : types.USER_LOGOUT,
});