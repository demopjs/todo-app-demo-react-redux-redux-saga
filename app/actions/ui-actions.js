// import * as types from 'constants/general-action-types';
import * as uiTypes from 'constants/ui-types';

const parents = document.querySelectorAll('body, html');

export const modalShow = (modalType, modalOptions) => {
  [].slice.call(parents).forEach((i) => i.classList.add('has-overlay'));
  return {
    type: uiTypes.MODAL_SHOW,
    modalType,
    modalOptions,
  };
};

export const modalHide = () => {
  [].slice.call(parents).forEach((i) => i.classList.remove('has-overlay'));
  return {
    type: uiTypes.MODAL_HIDE,
  };
};

export const setFocusOnInputById = (id) => {
  const input = document.getElementById(id);
  input.focus();
  return {
    type: uiTypes.INPUT_FOCUS,
  };
}