import React, { Component, PropTypes } from 'react';

class Footer extends Component {

  render() {
    const style = require('./Footer.styl');
    return (
      <div className={style.container}>
        Списки в которые я заходил
      </div>
    );
  }

}


Footer.propTypes = {
  className: PropTypes.string,
};


export default Footer;
