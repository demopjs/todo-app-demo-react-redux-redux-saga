import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import findIndex from 'lodash/findIndex';

import Helmet from 'react-helmet';

import Listing from 'containers/Listing/Listing';
import Login from 'containers/Login/Login';
import ListInfo from 'containers/ListInfo/ListInfo';
import Tasks from 'containers/Tasks/Tasks';
import Modals from 'containers/Modals/Modals';
import Footer from 'containers/Footer/Footer';
import Sharing from 'containers/Sharing/Sharing';
import UserTag from 'components/UserTag/UserTag';

import * as apiActions from 'actions/api-actions';

import style from './App.styl';

import { decodeCallback, encodeCallback } from 'modules/callback-utils';


class App extends Component { 

  constructor(props) {
    super(props);
    this.processCurrentUser = this.processCurrentUser.bind(this);
  }

  componentWillMount() {
    // this.processCurrentRoute();
    // this.checkRouteId();
    // this.props.fetchUser();
  }

  componentDidMount() {
    this.processCallback(this.processCurrentUser);
    // this.props.fetchAllLists();
  }

  componentWillUpdate() {}

  componentDidUpdate() {
    this.processCurrentRoute();
  }


  processCurrentUser() {
    this.props.fetchUser();
  }

  processCallback(cb) {
    const { router } = this.props;
    const { callback } = router.location.query;
    if(callback) {
      const callbackData = decodeCallback(callback);
      if(callbackData && callbackData.url) {
        router.push(callbackData.url);
      }
      if(callbackData && callbackData.action) {
        const actionToRun = this.props[callbackData.action].bind(this);
        // TODO: figure out how to run this action properly
        // after lists and user are fetched
        setTimeout(() => {
          console.log('execute an action', callbackData.action, 'with options', callbackData.options);
          const callbackOptions = callbackData.options || [];
          actionToRun(...callbackOptions);
        }, 1000);
      }
      else {
        console.warn('Got callback but could not process it', callbackData);
      }
    }
    cb();
  }

  processCurrentRoute() {
    // with this method we want only to be sure that 
    // we have requested list in the store
    // if no lists requested (not a /list/:id, eg / route)
    const { router, routeParams, fetchLists, lists, user } = this.props;

    const isListIdRoute = /\/list\/\S+/.test(router.location.pathname);

    
    // if it is a list/:id route, check if it exists in the store
    if(isListIdRoute && lists && lists.length) {
      const index = this.getCurrentListIndex();
      if(index >= 0) {
        // route is one of the lists, do nothing
      } else if(routeParams && routeParams.id) {
        // route is NOT one of the lists in the stode
        // we have an id so request a list
        fetchLists(routeParams.id);
      } else {
        // we have no list id, but it is an isListIdRoute
        // so probably an error occured
        // This should never happer in reality.
        console.error('List ID conflict. It is a list route but no list id in react-router.');
      }
    }
    else if (lists && lists.length) {
      // NOT an isListIdRoute, but have lists
      // move to the first list then
      const currentListId = lists[0].id;
      if(currentListId) {
        router.push(`/list/${currentListId}`);
      } else {
        fetchLists();
      }
    } 
    else if (isListIdRoute && routeParams.id) {
      // anything else - means we should fetch a list
      fetchLists(routeParams.id);
    } else {
      fetchLists();
    }
  }


  getCurrentListIndex() {
    const { routeParams, lists } = this.props;
    if(!routeParams || !routeParams.id || !lists || !lists.length) return false;
    return findIndex(lists, (i) => (i.id === routeParams.id));
  }


  // TODO: analyze callback param on componentDidMount only
  // do not need to do it on component update, because possible only as a callback


  checkRouteId() {
    const { router, routeParams, lists, user } = this.props;
    const isNewListRoute = /\/new-list/.test(router.location.pathname);
    const isListIdRoute = /\/list\/\S+/.test(router.location.pathname);

    if (!isListIdRoute && lists && lists[0] && lists[0].id) {
      router.push(`/list/${lists[0].id}`);
    } 
  }

  getListsData() {
    const { routeParams, lists, user } = this.props;
    const currentListIndex = this.getCurrentListIndex();
    if(currentListIndex >= 0) {
      return lists.map((i) => ({
        ...i,
        isActive: (i.id === routeParams.id),
      }));
    } 
    return null;
  }


  getCurrentListData() {
    // const { routeParams } = this.props;
    const lists = this.getListsData();
    if (lists && lists.length > 1) {
      return lists.filter((i) => (i.isActive))[0];
    } else if (lists && lists.length === 1) {
      return lists[0];
    } 
    return null;
  }


  renderModals() {
    const { isModalShown, modalOptions, modalType } = this.props.ui;
    const currentListData = this.getCurrentListData() || {};
    if(isModalShown) {
      return (
        <Modals 
          currentList={currentListData} 
          type={modalType} 
          options={modalOptions} 
        />
      );
    }
    return null;
  }
  

  render() {
    const { router, routeParams, user } = this.props;
    
    const lists = this.getListsData() || [];
    const currentListData = this.getCurrentListData() || {};
    const { ownername, ownerUrl, ownerSN } = currentListData;

    const isOwner = currentListData.owned || false;
    const isGuest = !user.logined;

    return (
      <div className={style.container}>

        <Helmet
          title={`${currentListData.name ? currentListData.name : 'Список'}${currentListData.ownername ? ' от ' + currentListData.ownername : ''}`}
        />

        <div className={style.listing}>
          <Listing 
            lists={lists}
            router={router} 
            user={user}
            isGuest={isGuest}
            isOwner={isOwner}
            routeParams={routeParams} 
          />
        </div>
        
        <div className={style.body}>
          <Login user={user} />
          {currentListData ? [
            <ListInfo 
              key="list-info-component" 
              listData={currentListData} 
              user={user}
              isGuest={isGuest}
              isOwner={isOwner}
            />,
            <Tasks 
              key="tasks-component" 
              listData={currentListData} 
              user={user}
              isGuest={isGuest}
              isOwner={isOwner}
            />,
          ] : 
            <p style={{ textAlign : 'center', margin: '20px' }}>Loading...</p>
          }
        </div>
        
        { !ownername || (
          <p style={{
            textAlign: 'center',
            margin: '5px 10px',
          }}>
            Владелец списка - 
            <UserTag name={ownername} url={ownerUrl} type={ownerSN} />
          </p>
        ) }
        
        <Sharing
          isGuest={isGuest}
          isOwner={isOwner}
          listData={currentListData}
        />

        <div className={style.footer}>
          <Footer />
        </div>

        {this.renderModals()}

      </div>
    );
  }

}


App.propTypes = {
  fetchLists     : PropTypes.func.isRequired,
  fetchUser     : PropTypes.func.isRequired,
  // fetchAllLists : PropTypes.func.isRequired,
  routeParams   : PropTypes.object.isRequired,
  router        : PropTypes.object.isRequired,
  lists         : PropTypes.array.isRequired,
  user          : PropTypes.object.isRequired,
  ui            : PropTypes.object.isRequired,
};


export default connect(
  (state) => ({ ...state }),
  (dispatch) => (bindActionCreators({ 
    ...apiActions,
  }, dispatch))
)(App);
