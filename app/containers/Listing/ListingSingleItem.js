import React, { Component, PropTypes } from 'react';

class ListingSingleItem extends Component {

  render() {
    const { id, onClick, className, isActive } = this.props;
    return (
      <li 
        id={id}
        onClick={onClick}
        className={`${className}${isActive ? ' is-active' : ''}`}
      >
        { this.props.children }
      </li>
    );
  }

}


ListingSingleItem.propTypes = {
  className : PropTypes.string,
  id        : PropTypes.string.isRequired,
  onClick   : PropTypes.func.isRequired,
  isActive  : PropTypes.bool,
  children  : PropTypes.node.isRequired,
};

export default ListingSingleItem;