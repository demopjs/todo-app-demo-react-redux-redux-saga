import React, { Component, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router';

import findIndex from 'lodash/findIndex';

import { addList } from 'actions/api-actions';
import { modalShow } from 'actions/ui-actions';
// import { updateUi, setActiveList } from 'actions/ui-actions';

import * as uiTypes from 'constants/ui-types';

import ListSingleItem from './ListingSingleItem';

import style from './Listing.styl';


class Listing extends Component {

  constructor(props) {
    super(props);
    this.document = document;

    this.state = { slideOffset: null };

    this.listItems = [];

    this.listingOffset = null;

    // swipe values
    this.boxInitialOffset = null;
    this.cursorInitialOffset = null;
    this.isDragging = false;

    // events binding
    this.handleListItemClick = this.handleListItemClick.bind(this);
    this.handleSwipeStart    = this.handleSwipeStart.bind(this);
    this.handleSwipeEnd      = this.handleSwipeEnd.bind(this);
    this.handleSwipeMove     = this.handleSwipeMove.bind(this);
    this.slideCarousel       = this.slideCarousel.bind(this);
    this.addItem             = this.addItem.bind(this);
    this.routeToListById     = this.routeToListById.bind(this);
  }

  componentDidMount() {
    this.setupSwipeEvents();
    // TODO: should launch on content loaded event instead if timeout
    setTimeout(() => {
      this.checkSlideOffset();
    }, 1);
  }

  componentDidUpdate() {
    const { lists, isGuest } = this.props;
    if(lists && lists.length && !isGuest) {
      this.checkSlideOffset();
    }
  }

  checkSlideOffset() {
    const currentSlideOffset = this.getslideOffset();
    if(!currentSlideOffset || currentSlideOffset === this.listingOffset) return;
    this.setListingOffset(currentSlideOffset);
  }

  setListingOffset(value) {
    this.listingOffset = value;
    this.itemsBox.style.left = value + 'px';
  }

  componentWillUnmount() {
    this.unsetSwipeEvents();
  }


  setupSwipeEvents() {
    this.document.addEventListener('touchstart', this.handleSwipeStart);
    this.document.addEventListener('touchend', this.handleSwipeEnd);
  }
  unsetSwipeEvents() {
    this.document.removeEventListener('touchstart', this.handleSwipeStart);
    this.document.removeEventListener('touchend', this.handleSwipeEnd);
  }

  handleListItemClick(id) {
    if (this.isDragging) return;
    this.routeToListById(id);
  }

  handleSwipeStart(e) {
    const el = e.target;
    if (el.dataset.carouselItemsBox !== undefined || el.closest('[data-carousel-items-box]')) {
      if(e.originalEvent && e.originalEvent.touches[0] && e.originalEvent.touches[0].pageX) {
        this.cursorInitialOffset = e.originalEvent.touches[0].pageX;
      } else if (e.touches && e.touches[0] && e.touches[0].pageX) {
        this.cursorInitialOffset = e.touches[0].pageX;
      } else {
        return;
        console.warn('Could not get touch pageX coords');
      }
      this.boxInitialOffset = parseInt(this.itemsBox.style.left, 10) || 0;
      this.document.addEventListener('touchmove', this.handleSwipeMove);
    }
    return;
  }
  handleSwipeEnd() {
    this.document.removeEventListener('touchmove', this.handleSwipeMove);
    if (this.isDragging) {
      this.boxInitialOffset = null;
      this.cursorInitialOffset = null;
      this.selectItemOnSwipeEnd();
      setTimeout(() => {
        this.isDragging = false;
      }, 10);
    } 
    return;
  }

  selectItemOnSwipeEnd() {
    const { lists } = this.props;
    const breakpoint = (this.itemsWrapper.offsetWidth / 2) - parseInt(this.itemsBox.style.left, 10);
    this.listItems.every((item, index) => {
      const itemNode = findDOMNode(item);
      const itemRightEdge = itemNode.offsetLeft + itemNode.offsetWidth;
      if (itemRightEdge > breakpoint) {
        this.routeToListById(lists[index].id);
        // this.selectCarouselItem(item);
        return false;
      } else if (index + 1 >= this.listItems.length) {
        this.routeToListById(lists[lists.length - 1].id);
        // this.selectCarouselItem(this.listItems[this.listItems.length - 1]);
        return false;
      } 
      return true;
    });
    // $items.each((item, index) => {
    //   
    
    // });
  }

  handleSwipeMove(e) {
    // e.preventDefault();
    let touchOffset = null;
    if(e.originalEvent && e.originalEvent.touches[0] && e.originalEvent.touches[0].pageX) {
      touchOffset = e.originalEvent.touches[0].pageX;
    } else if (e.touches && e.touches[0] && e.touches[0].pageX) {
      touchOffset = e.touches[0].pageX;
    } else {
      console.warn('Could not get touch pageX coords');
      return;
    }
    
    // in case of an accidental move
    if (Math.abs(touchOffset - this.cursorInitialOffset) > 5) {
      this.isDragging = true;
      const offsetDiff = touchOffset - this.cursorInitialOffset;
      const boxOffset = this.boxInitialOffset + offsetDiff;
      this.setListingOffset(boxOffset);
    } 
    return;
  }


  addItem() {
    const { addList, user, isGuest, isOwner, router, modalShow } = this.props;
    if(!isGuest) {
      if(isOwner) {
        addList();
      } else {
        addList({ shouldFetchListsFirst: true });
      }
      return;
    }
    modalShow(uiTypes.MODAL_LOGIN, { callback: { action: 'addList' } });
  }

  slideCarousel(prev = false) {
    const { lists, routeParams } = this.props;
    const activeIndex = findIndex(lists, (i) => (i.id === routeParams.id));
    const totalItems = lists.length;

    if(prev && activeIndex > 0) {
      this.routeToListById(lists[activeIndex - 1].id);
    } 
    else if (!prev && activeIndex < totalItems - 1) {
      this.routeToListById(lists[activeIndex + 1].id);
    } 
    else {
      return;
    }
  }


  // GENERAL
  getslideOffset() {
    const { routeParams } = this.props;
    let itemComponent = null;
    if (routeParams && routeParams.id) {
      itemComponent = this.findListComponentById(routeParams.id);
    }
    if(!itemComponent) return false;

    const item = findDOMNode(itemComponent);
    if(!item) return false;
    const boxWidth = this.itemsWrapper.offsetWidth;
    const itemWidth = item.offsetWidth;
    const itemOffset = item.offsetLeft;
    return ((boxWidth / 2) - itemOffset) - (itemWidth / 2);
    // this.props.setActiveList(item.id);
  }

  routeToListById(id) {
    const { router } = this.props;
    router.push('/list/' + id);
  }

  findListComponentById(id) {
    const { lists } = this.props;
    if(lists && lists.length) {
      let index = findIndex(lists, (i) => (i.id === id));
      index = (index < 0) ? 0 : index;
      return this.listItems[index];
    }
    return false;
  }


  renderListItems() {
    const { lists, routeParams } = this.props;
    const refs = [];
    let items = null;
    
    if (lists && lists.length) {
      items = lists.map((list) => {
        const isActive = list.id === routeParams.id;
        return (
          <ListSingleItem 
            key={list.id}
            id={list.id}
            ref={(i) => { refs.push(i); }}
            isActive={isActive}
            onClick={() => { this.handleListItemClick(list.id); }}
            className={isActive ? style.singleItemActive : style.singleItem}
          >
            <Link 
              style={{ 
                color: list.owned ? '' : 'yellow',
              }} 
              to={`/list/${list.id}`}
            >
              {list.name || 'Безымянный'}
            </Link>
          </ListSingleItem>
        );
      });
    }
    this.listItems = refs;
    return items;
  }


  render() {
    const { slideOffset } = this.state;
    
    const iconNavPrev = require('./icons/nav-prev.svg');
    const iconNavNext = require('./icons/nav-next.svg');
    const iconPlus = require('./icons/plus.svg');

    const { lists, isOwner, isGuest } = this.props;

    return (
      <div className={style.listWrapper}>
        <div className={style.carousel}>
          {lists && lists.length && !isGuest ? [
            (<button
              key="listing-go-prev-component"
              onClick={() => { this.slideCarousel(true); }}
              ref={(i) => { this.navPrev = i; }}
              className={style.navPrev}
            >
              <img src={iconNavPrev} alt="" />
            </button>),

            (<div
              key="listing-carousel-component"
              ref={(i) => { this.itemsWrapper = i; }}
              className={style.content}
            >
              <ul
                data-carousel-items-box
                ref={(i) => { this.itemsBox = i; }}
                className={style.itemsList}
                /*style={{ left : `${slideOffset || 0}px`, }}*/
                style={{ left: `${this.listingOffset || 0}px` }}
              >
                {this.renderListItems()}
              </ul>
            </div>),

            (<button
              key="listing-go-next-component"
              onClick={() => { this.slideCarousel(); }}
              ref={(i) => { this.navNext = i; }}
              className={style.navNext}
            >
              <img src={iconNavNext} alt="" />
            </button>),
          ] : (<div>&nbsp;</div>)}

          <button
            onClick={() => { this.addItem(); }}
            ref={(i) => { this.addButton = i; }}
            className={style.addItem}
          >
            <img src={iconPlus} alt="" />
          </button>
        </div>
      </div>
    );
  }
}


Listing.propTypes = {
  lists       : PropTypes.array,
  user        : PropTypes.object,
  addList     : PropTypes.func.isRequired,
  modalShow   : PropTypes.func.isRequired,
  routeParams : PropTypes.object,
  router      : PropTypes.object,
  isGuest     : PropTypes.bool,
  isOwner     : PropTypes.bool,
};


export default connect(
  // (state) => ({ }),
  null,
  (dispatch) => (bindActionCreators({ addList, modalShow }, dispatch))
)(Listing);
