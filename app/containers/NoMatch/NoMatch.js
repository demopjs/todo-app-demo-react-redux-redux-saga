import React, { PropTypes } from 'react';

const NoMatch = (props) => {
  return (
    <div>NoMatch. 404 Error!</div>
  );
};

NoMatch.propTypes = {
  className: PropTypes.string,
};

export default NoMatch;
