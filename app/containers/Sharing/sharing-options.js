export default [
  // https://twitter.com/intent/tweet?text=%TITLE%&url=%URL%
  // https://plus.google.com/share?url=%URL%
  // http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=%URL%&st.comments=%TITLE%
  {
    id         : 'vk',
    iconUrl    : require('assets/icons/vk.svg'),
    sharingUrl : 'https://vk.com/share.php?url=%URL%&title=%TITLE%&noparse=true',
  },
  {
    id         : 'fb',
    iconUrl    : require('assets/icons/facebook.svg'),
    sharingUrl : 'https://www.facebook.com/sharer.php?u=%URL%',
  },
  {
    id         : 'ok',
    iconUrl    : require('assets/icons/odnoklassniki.svg'),
    sharingUrl : 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=%URL%&st.comments=%TITLE%',
  },
];