import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { modalShow } from 'actions/ui-actions';
import * as uiTypes from 'constants/ui-types';

import sharingOptions from './sharing-options';
import style from './Sharing.styl';

class Sharing extends Component {

  constructor(props) {
    super(props);

    this.handleLinkClick = this.handleLinkClick.bind(this);
    this.handleShareWithLinkInputClick = this.handleShareWithLinkInputClick.bind(this);
    this.handleShareWithLinkClick = this.handleShareWithLinkClick.bind(this);
  }

  handleShareWithLinkInputClick(e) {
    e.target.select();
  }

  handleLinkClick(url) {
    const { isGuest, modalShow } = this.props;
    const { pathname } = window.location;
    if(isGuest) {
      modalShow(uiTypes.MODAL_LOGIN, { 
        callback: { url: pathname },
      });
    }
    else {
      window.open(url);
    }
  }

  handleShareWithLinkClick() {
    const { isGuest, modalShow } = this.props;
    const { pathname } = window.location;
    modalShow(uiTypes.MODAL_LOGIN, { 
      callback: { url: pathname },
    });
  }

  renderLinks() {
    const pageUrl = window.location.href;
    const { name } = this.props.listData;

    return sharingOptions.map((item) => {
      const encodedUrl = encodeURI(pageUrl);
      const encodedName = encodeURI(name);
      const pathToShare = item.sharingUrl.replace('%URL%', encodedUrl).replace('%TITLE%', encodedName);
      return (
        <a 
          onClick={() => { this.handleLinkClick(pathToShare) }} 
          className={style.singleLink} 
          href='javascript:void(0)'
          key={item.id}
        >
          <img src={item.iconUrl} alt="" />
        </a>
      );
    });
  }

  render() {

    const { isGuest } = this.props;
    
    return (
      <div className={style.container}>

        <div className={style.title}>поделиться</div>
        <div className={style.links}>
          {this.renderLinks()}
        </div>

        <div className={style.sharignWithLink}>
          {isGuest ? (
            <a onClick={this.handleShareWithLinkClick} href="javascript:void(0)">Или нажми сюда чтобы получить ссылку на эту страницу</a>
          ) : ([
            <p key="para">Или скопируй и отправь эту ссылку</p>,
            <input 
              onClick={this.handleShareWithLinkInputClick}
              onChange={() => {}} 
              key="input" 
              type="text" 
              value={window.location.href} 
            />
          ])}
        </div>

      </div>
    );
  }

}


Sharing.propTypes = {
  isGuest   : PropTypes.bool,
  listData  : PropTypes.object,
  modalShow : PropTypes.func.isRequired,
};


export default connect(
  // (state) => ({ }),
  null,
  (dispatch) => (bindActionCreators({ modalShow }, dispatch))
)(Sharing);
