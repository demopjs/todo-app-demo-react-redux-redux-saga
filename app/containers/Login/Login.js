import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { modalShow } from 'actions/ui-actions';

import * as uiTypes from 'constants/ui-types';

import style from './Login.styl';

class Login extends Component {

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { user, modalShow } = this.props;
    if(user.logined) {
      modalShow(uiTypes.MODAL_LOGIN, { text: 'Добавьте ещё один аккаунт' });
    } else {
      const url = window.location.pathname;
      modalShow(uiTypes.MODAL_LOGIN, { callback: { url } });
    }
  }

  render() {
    const userIcon = require('./icons/user.svg');
    const { username, logined } = this.props.user;
    return (
      <div className={style.user}>
        <div onClick={this.handleClick} className={style.action}>
          <img className={style.icon} src={userIcon} alt="" />
          <span className={style.name}>
            { !logined ? 'Login' : username || 'Username' }
          </span>
        </div>
      </div>
    );
  }

}


Login.propTypes = {
  user   : PropTypes.object.isRequired,
  modalShow  : PropTypes.func.isRequired,
};


export default connect(
  null,
  (dispatch) => (bindActionCreators({ modalShow }, dispatch))
)(Login);