import React, { Component, PropTypes } from 'react';

import * as uiTypes from 'constants/ui-types';

import ModalWrapper from './ModalWrapper';

import ModalLogin from './ModalLogin';
import ModalTaskEdit from './ModalTaskEdit';
import ModalText from './ModalText';

class Modals extends Component {

  render() {
    const { type, options, currentList } = this.props;

    switch (type) {
      case uiTypes.MODAL_LOGIN :
        return <ModalWrapper><ModalLogin {...options} /></ModalWrapper>;

      case uiTypes.MODAL_TASK_EDIT :
        return (
          <ModalWrapper>
            <ModalTaskEdit currentList={currentList} {...options} />
          </ModalWrapper>
        );

      case uiTypes.MODAL_TEXT :
        return <ModalWrapper><ModalText {...options} /></ModalWrapper>;

      case uiTypes.MODAL_TASK_INFO :
        return <ModalWrapper><ModalText {...options} /></ModalWrapper>;

      default :
        return null;
    }
  }
}


Modals.propTypes = {
  type        : PropTypes.string.isRequired,
  options     : PropTypes.object.isRequired,
  currentList : PropTypes.object.isRequired,
};


export default Modals;
