import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { userLogin } from 'actions/api-actions';

import loginOptions from './login-options';

class ModalLogin extends Component {

  constructor(props) {
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
  }

  handleLoginClick(service) {
    const callback = this.props.callback || false;
    userLogin(service, callback);
  }

  render() {
    const style = require('./Modals.styl');

    // const icons = [
    //   require('assets/icons/vk.svg'),
    //   require('assets/icons/facebook.svg'),
    //   require('assets/icons/google-plus.svg'),
    //   require('assets/icons/livejournal.svg'),
    //   require('assets/icons/odnoklassniki.svg'),
    //   require('assets/icons/twitter.svg'),
    // ];

    return (
      <div className={style.loginModal}>
        <p>
          { this.props.text ? 
              this.props.text :
              'Зарегистрируйтесь через одну из соцсетей чтобы совершить действие.'
          }
        </p>
        <div className={style.loginOptions}>
          {loginOptions.map((i, index) => (
            <button 
              key={index} 
              onClick={ () => { this.handleLoginClick(i.urlId) } }
              // href={`/auth/${i.urlId}${callback ? '?callback=' + callback : '' }`}
            >
              <img src={i.iconUrl} alt="" />
            </button>
          ))}
        </div>
      </div>
    );
  }

}


ModalLogin.propTypes = {
  callback: PropTypes.object,
  text : PropTypes.string,
};


export default connect(
  null,
  (dispatch) => (bindActionCreators({ userLogin }, dispatch))
)(ModalLogin);