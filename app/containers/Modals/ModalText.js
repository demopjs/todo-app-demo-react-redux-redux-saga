import React, { Component, PropTypes } from 'react';

class ModalText extends Component {

  render() {
    const { text } = this.props;
    const style = require('./Modals.styl');
    return (
      <div className={style.textModal}>
        <p>{text || 'Нет сообщения'}</p>
      </div>
    );
  }

}


ModalText.propTypes = {
  text: PropTypes.string,
};


export default ModalText;
