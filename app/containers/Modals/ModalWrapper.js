import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { modalHide } from 'actions/ui-actions';

class ModalWrapper extends Component {

  constructor(props) {
    super(props)
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.props.modalHide();
  }

  render() {
    const style = require('./Modals.styl');
    const iconClose = require('assets/icons/close.svg');
    return (
      <div className={style.overlay}>

        <div className={style.modal}>

          <button onClick={this.handleClose} className={style.modalClose}>
            <img src={iconClose} alt="" />
          </button>

          <div className={style.modalBody}>
            {this.props.children}
          </div>

        </div>

      </div>
    );
  }

}


ModalWrapper.propTypes = {
  children  : PropTypes.node,
  modalHide : PropTypes.func.isRequired,
};


export default connect(
  // (state) => ({ }),
  null,
  (dispatch) => (bindActionCreators({ modalHide }, dispatch))
)(ModalWrapper);

