export default [
  {
    urlId: 'vkontakte',
    iconUrl: require('assets/icons/vk.svg'),
  },
  {
    urlId: 'facebook',
    iconUrl: require('assets/icons/facebook.svg'),
  },
  {
    urlId: 'odnoklassniki',
    iconUrl: require('assets/icons/odnoklassniki.svg'),
  },
  // {
  //   urlId: '',
  //   iconUrl: require('assets/icons/google-plus.svg'),
  // },
  // {
  //   urlId: '',
  //   iconUrl: require('assets/icons/livejournal.svg'),
  // },
  // {
  //   urlId: '',
  //   iconUrl: require('assets/icons/twitter.svg'),
  // },
];

// ?callback={url: '/list/listid-209348', action: 'myActionName', options: ['asiohten', {i: 3}]}