import React, { Component, PropTypes } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { updateTask, deleteTask } from 'actions/api-actions';
import { modalHide } from 'actions/ui-actions';

class ModalTaskEdit extends Component {

  constructor(props) {
    super(props);

    this.handleTaskNameChange = this.handleTaskNameChange.bind(this);
    this.handleTaskDescriptionChange = this.handleTaskDescriptionChange.bind(this);
    this.handleDeleteTask = this.handleDeleteTask.bind(this);
  }

  handleTaskNameChange(taskId) {
    const { updateTask, currentList } = this.props;
    updateTask(currentList.id, {
      id: taskId,
      name: this.inputName.value || '',
    });
  }
  handleTaskDescriptionChange(taskId) {
    const { updateTask, currentList } = this.props;
    updateTask(currentList.id, {
      id: taskId,
      description: this.inputDescription.value || '',
    });
  }
  handleDeleteTask(taskId) {
    const { deleteTask, currentList, modalHide } = this.props;
    deleteTask(currentList.id, taskId);
    modalHide();
  }

  render() {
    const style = require('./Modals.styl');

    const { taskId, currentList, editable, text } = this.props;
    const currentTask = currentList.tasks.filter((i) => (i.id === taskId))[0];

    return (
      <div className={style.taskModal}>
        <div className={style.taskEditRow}>
          <span className={style.taskEditRowLabel}>Название</span>
          <input 
            disabled={!editable}
            ref={ (ref) => (this.inputName = ref) }
            type="text" 
            onChange={ () => { this.handleTaskNameChange(currentTask.id) }}
            value={currentTask.name}
          />
        </div>
        <div className={style.taskEditRow}>
          <span className={style.taskEditRowLabel}>Описание</span>
          <textarea 
            disabled={!editable}
            ref={ (ref) => (this.inputDescription = ref) }
            onChange={ () => { this.handleTaskDescriptionChange(currentTask.id) }}
            value={currentTask.description}
          ></textarea>
        </div>
        {text && (
          <div className={style.taskEditRow}>
            <div dangerouslySetInnerHTML={{ __html: text }} />
          </div>
        )}

        {editable && (
          <button onClick={() => { this.handleDeleteTask(currentTask.id) }} className={style.taskEditDelete}>
            Удалить из списка
          </button>
        )}
        
      </div>
    );
  }

}


ModalTaskEdit.propTypes = {
  // tasks       : PropTypes.array.isRequired,
  // listId      : PropTypes.string,
  taskId      : PropTypes.string,
  currentList : PropTypes.object.isRequired,
  updateTask  : PropTypes.func.isRequired,
  deleteTask  : PropTypes.func.isRequired,
  modalHide   : PropTypes.func.isRequired,
  editable    : PropTypes.bool,
  text        : PropTypes.string,
};


export default connect(
  // (state) => ({ }),
  null,
  (dispatch) => (bindActionCreators({ updateTask, deleteTask, modalHide }, dispatch))
)(ModalTaskEdit);
