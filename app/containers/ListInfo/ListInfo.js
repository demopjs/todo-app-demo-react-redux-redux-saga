import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateList } from 'actions/api-actions';
import { setFocusOnInputById } from 'actions/ui-actions';

class ListInfo extends Component {

  constructor(props) {
    super(props);
    this.handleListNameChange = this.handleListNameChange.bind(this);
    this.setFocusToTheInput = this.setFocusToTheInput.bind(this);
    this.handleListNameKeyUp = this.handleListNameKeyUp.bind(this);
  }

  componentDidMount() {
    this.setFocusToTheInput('list-name-input');
  }
  componentDidUpdate() {
    this.setFocusToTheInput('list-name-input');
  }

  shouldComponentUpdate(nextProps, nextState) {
    const l1 = this.props.listData;
    const l2 = nextProps.listData;
    if(l1.id === l2.id && l1.name === l2.name) return false;
    return true;
  }

  setFocusToTheInput(id) {
    setTimeout(() => {
      this.props.setFocusOnInputById(id);
    }, 100);
  }

  handleListNameChange() {
    const { updateList, listData } = this.props;
    updateList({
      id   : listData.id,
      name : this.listNameInput.value || '',
    });
  }

  handleListNameKeyUp(e) {
    if(e.nativeEvent.keyCode === 13) {
      this.setFocusToTheInput('add-task-input');
    }
  }

  render() {
    const style = require('./ListInfo.styl');
    const { isOwner, user, listData } = this.props;
    return (
      <div className={style.container}>

        <input 
          id="list-name-input"
          autoFocus
          onChange={this.handleListNameChange} 
          onKeyUp={this.handleListNameKeyUp}
          className={style.listTitle} 
          disabled={!isOwner}
          type="text" 
          placeholder="Имя списка"
          value={listData.name || ''}
          ref={ (ref) => { this.listNameInput = ref } }
        />

      </div>
    );
  }

}


ListInfo.propTypes = {
  listData            : PropTypes.object.isRequired,
  isOwner             : PropTypes.bool,
  user                : PropTypes.object.isRequired,
  updateList          : PropTypes.func.isRequired,
  setFocusOnInputById : PropTypes.func,
};


export default connect(
  null,
  (dispatch) => (bindActionCreators({ updateList, setFocusOnInputById }, dispatch))
)(ListInfo);