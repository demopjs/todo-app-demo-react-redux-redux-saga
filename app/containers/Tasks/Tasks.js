import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { updateTask, updateTaskIndex, addTask } from 'actions/api-actions';
import { modalShow } from 'actions/ui-actions';

import * as uiTypes from 'constants/ui-types';

import SingleTask from './SingleTask';

const style = require('./Tasks.styl');

class Tasks extends Component {

  constructor(props) {
    super(props);

    this.state = {
      newTaskName : '',
    };

    this.handleTaskActionClick = this.handleTaskActionClick.bind(this);
    this.handleAddTaskChange   = this.handleAddTaskChange.bind(this);
    this.handleAddTaskKeyUp    = this.handleAddTaskKeyUp.bind(this);
    this.handleIndexClick      = this.handleIndexClick.bind(this);
  }

  handleIndexClick(taskId, listId) {
    const { updateTaskIndex, modalShow, isOwner, isGuest, listData, user } = this.props;
    const { tasks } = listData;
    const currentTask = tasks.filter((i) => (i.id === taskId))[0];
    const currentTaskState = currentTask.completed || false;

    if(isOwner || currentTask.owned) {
      updateTaskIndex(listId, {
        ...currentTask,
        completed : !currentTaskState,
      });
    } 
    else if (isGuest) {
      modalShow(uiTypes.MODAL_LOGIN, {
        callback : {
          url: window.location.pathname,
          action: 'updateTaskIndex',
          options: [
            listId,
            {
              ...currentTask,
              completed : !currentTaskState,
            },
          ],
        },
      });
    }
    else if (!currentTaskState) {
      updateTaskIndex(listId, {
        ...currentTask,
        completed : true,
      });
    }
    else {
      modalShow(uiTypes.MODAL_TEXT, { text: 'Нельзя снимать отметку с чужих задач.' });
    }
  }

  handleTaskActionClick(taskId, listId) {
    const { listData, isGuest, isOwner, user, updateTask, modalShow } = this.props;
    const { tasks } = listData;
    const currentTask = tasks.filter((i) => (i.id === taskId))[0];

    const isTaskOwned = currentTask.owned;
    const isTaskCompleted = currentTask.completed;

    const icons = {
      vkontakte     : require('assets/icons/vkontakte.svg'),
      facebook      : require('assets/icons/facebook.svg'),
      odnoklassniki : require('assets/icons/odnoklassniki.svg'),
    };

    if(isGuest) { // 1
      modalShow(uiTypes.MODAL_LOGIN, {
        callback: { url: window.location.pathname },
      });
    }
    else if(isOwner && !isTaskCompleted) { // 2
      modalShow(uiTypes.MODAL_TASK_EDIT, { taskId, listId, editable: true });
      // --- Название + описание (edit)
      // --- Кнопка удалить
    }
    else if(isOwner && isTaskCompleted && isTaskOwned) { // 3
      modalShow(uiTypes.MODAL_TASK_EDIT, { 
        taskId, 
        listId, 
        editable: false,
        text : `<p>Эту задачу выполню я</p> <p><i>* Вы не можете изменять эту задачу, так как она уже выполняется</i></p>`,
      });
      // --- Название + описание (read)
      // --- текст (Эту задачу выполню я)
      // --- текст (* Вы не можете изменять эту задачу, так как она уже выполняется)
    }
    else if(isOwner && isTaskCompleted && !isTaskOwned) { // 4
      const ownerIcon = currentTask.ownerSN ? `
        <span class="${style.ownerIcon}">
          <img src="${icons[currentTask.ownerSN]}"/>
        </span>` : '';
      const ownerName = currentTask.ownername ? currentTask.ownername : '%ownername%';
      const ownerBlock = `
        <a href="${currentTask.ownerUrl || '#'}">
          ${ownerIcon}<span>${ownerName}</span>
        </a>
      `;
      modalShow(uiTypes.MODAL_TASK_EDIT, { 
        taskId, 
        listId, 
        editable: false,
        text : `<p>Эту задачу выполняет ${ownerBlock}</p> <p><i>* Вы не можете изменять эту задачу, так как она уже выполняется</i></p>`,
      });
      // --- Название + описание (read)
      // --- текст (Эту задачу выполняет {ownerName})
      // --- text (* Вы не можете изменять эту задачу, так как она уже выполняется)
    }
    else if(!isOwner && !isTaskCompleted) { // 5
      modalShow(uiTypes.MODAL_TASK_EDIT, { 
        taskId, 
        listId, 
        editable: false,
        text : `<p><i>* Вы не можете изменять эту задачу, так как она чужая</i></p>`,
      });
      // --- Название + описание (read)
      // --- text (* Вы не можете изменять эту задачу, так как она чужая)
    }
    else if(!isOwner && isTaskCompleted && isTaskOwned) { // 6
      modalShow(uiTypes.MODAL_TASK_EDIT, { 
        taskId, 
        listId, 
        editable: false,
        text : `<p><i>* Вы не можете изменять эту задачу, так как она уже выполняется</i></p>`,
      });
      // --- Название + описание (read)
      // --- text (* Вы не можете изменять эту задачу, так как она уже выполняется)
    }
    else if(!isOwner && isTaskCompleted && !isTaskOwned) { // 7
      modalShow(uiTypes.MODAL_TASK_EDIT, { 
        taskId, 
        listId, 
        editable: false,
        text : `<p><i>* Вы не можете изменять эту задачу, так как она уже выполняется</i></p>`,
      });
      // --- Название + описание (read)
      // --- text (* Вы не можете изменять эту задачу, так как она уже выполняется)
    } 
    else {
      console.warn(`Unknown condition on action. SPECS - isOwner: "${isOwner}", isTaskCompleted: "${isTaskCompleted}", isTaskOwned: "${isTaskOwned}", taskId: "${taskId}", listId: "${listId}". AND SHUT THE FUCK UP! :>`);
    }
    
  }

  handleAddTaskChange() {
    this.setState({
      newTaskName: this.addTaskInput.value || '',
    });
  }

  handleAddTaskKeyUp(e) {
    if(e.nativeEvent.keyCode === 13 && this.addTaskInput.value) {
      const { listData, addTask } = this.props;
      addTask(listData.id, {
        name       : this.addTaskInput.value,
        completed  : false,
        ownername  : "",
        listId     : listData.id,
      });
      this.setState({
        newTaskName : '',
      });
    }
  }

  renderTasks() {
    const { listData } = this.props;
    const { tasks } = listData;
    if(tasks && tasks.length) {
      return tasks.map((task, index) => (
        <SingleTask 
          key={task.id}
          id={task.id}
          name={task.name}
          onIndexClick={() => this.handleIndexClick(task.id, listData.id)}
          onActionClick={() => this.handleTaskActionClick(task.id, listData.id)}
          description={task.description} 
          index={(index + 1).toString()} 
          isComplete={task.completed}
        />
      ));
    }
    return null;
  }

  render() {
    const tasks = this.renderTasks();

    const { isOwner, isGuest } = this.props;
    const { newTaskName } = this.state;
    return (
      <div className={style.container}>
        {
          isOwner ? (
            <input 
              id="add-task-input"
              className={style.addTask}
              type="text" 
              onChange={this.handleAddTaskChange}
              onKeyUp={this.handleAddTaskKeyUp}
              ref={(ref) => { this.addTaskInput = ref }}
              placeholder="Имя задачи" 
              value={newTaskName}
            />
          ) : isGuest ? (
            <div className={style.description}>Залогиньтесь, чтобы работать с задачами.</div>
          ) : (
            <div className={style.description}>Отметьте задачу, которую будете выполнять.</div>
          )
        }
        
        <div className={style.tasks}>
          {tasks || (<p style={{ textAlign: 'center', padding: '10px' }}>Нет задач</p>)}
        </div>
      </div>
    );
  }

}


Tasks.propTypes = {
  listData   : PropTypes.object.isRequired,
  isGuest    : PropTypes.bool,
  isOwner    : PropTypes.bool,
  user       : PropTypes.object.isRequired,
  updateTask : PropTypes.func.isRequired,
  updateTaskIndex : PropTypes.func.isRequired,
  addTask    : PropTypes.func.isRequired,
  modalShow  : PropTypes.func.isRequired,
};


export default connect(
  null,
  (dispatch) => (bindActionCreators({ updateTask, addTask, modalShow, updateTaskIndex }, dispatch))
)(Tasks);
