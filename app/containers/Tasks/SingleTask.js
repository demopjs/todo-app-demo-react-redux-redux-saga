import React, { Component, PropTypes } from 'react';

class SingleTask extends Component {

  render() {
    const style = require('./Tasks.styl');
    const { isComplete, name, description, index } = this.props;

    const iconCheck = require('./icons/check.svg');
    const iconEllipsis = require('./icons/ellipsis.svg');

    return (
      <div className={isComplete ? style.completedTask : style.defaultTask}>
        <button onClick={this.props.onIndexClick} className={style.taskIndex}> 
          {index} 
          {isComplete ? <img className={style.taskChecked} src={iconCheck} alt="" /> : ''}
        </button>

        <div className={style.taskContent}>
          <h2 className={style.taskName}>
            {name}
          </h2>
          {description ? <div className={style.taskDescription}>{description}</div> : '' }
        </div>

        <div className={style.taskActions}>
          <button onClick={this.props.onActionClick}>
            <img src={iconEllipsis} alt="" />
          </button>
        </div>

      </div>
    );
  }

}


SingleTask.propTypes = {
  name          : PropTypes.string.isRequired,
  index         : PropTypes.string.isRequired,
  description   : PropTypes.string,
  isComplete    : PropTypes.bool,
  onActionClick : PropTypes.func.isRequired,
  onIndexClick  : PropTypes.func.isRequired,
};


export default SingleTask;
