import React, { Component, PropTypes } from 'react';
import style from './UserTag.styl';

const icons = {
  vkontakte     : require('assets/icons/vkontakte.svg'),
  facebook      : require('assets/icons/facebook.svg'),
  odnoklassniki : require('assets/icons/odnoklassniki.svg'),
};

class UserTag extends Component {

  render() {
    const { name, type, url } = this.props;
    
    const userName = name || 'НЕТ ИМЕНИ';
    const href = url || '#';
    return (
      <a href={href} className={style.container}>
        { (!type || !icons[type]) || (
          <span className={style.icon}>
            <img src={icons[type]} alt="" />
          </span>
        ) }
        <span className={style.name}>{userName}</span>
      </a>
    );
  }

}


UserTag.propTypes = {
  name : PropTypes.string,
  type : PropTypes.string,
  url  : PropTypes.string,
};


export default UserTag;
