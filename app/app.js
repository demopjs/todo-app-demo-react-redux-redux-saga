// import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import reducers from 'reducers';

import createSagaMiddleware  from 'redux-saga';
import { rootSaga } from 'sagas/root-saga';

import AppRouter from './AppRouter';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [];
middlewares.push(applyMiddleware(sagaMiddleware));
if(window.__REDUX_DEVTOOLS_EXTENSION__) {
  middlewares.push(window.__REDUX_DEVTOOLS_EXTENSION__());
}

const store = createStore(reducers, compose(...middlewares));

sagaMiddleware.run(rootSaga);


ReactDOM.render(
  <Provider store={store}>
    <AppRouter />
  </Provider>,
  document.querySelector('#app')
);
