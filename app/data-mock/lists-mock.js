import user from './user-mock';

const lists = [];
const listsTotal = 5;
const tasksPerList = 5;


// create lists
for (let i = 0; i < listsTotal; i++) {
  const listId = 'listid-' + i;
  const listMock = { 
    id        : listId,
    name      : 'Список ' + Math.floor(Math.random() * 4792572),
    ownerId   : 1,
    ownerName : 'Кирилл Сабитов',
    createdAt : new Date(),
    tasks     : [],
  };

  // create tasks
  for (let i2 = 0; i2 < tasksPerList; i2++) {
    const taskId = 'taskid-' + Math.floor(Math.random() * 982498235);
    listMock.tasks.push({
      id          : taskId,
      name        : 'Задача ' + Math.floor(Math.random() * 19234872),
      description : (i2 % 2 === 1) ? 'Lorem ipsum ' + Math.floor(Math.random() * 19234872) : '',
      isComplete  : i2 % 2 === 1,
      assignedTo  : i2 % 2 === 1 ? user : null,
      createdAt   : new Date(),
      listId,
    });
    // listMock.tasks.push(taskId);
  }
  lists.push(listMock);
}


export default lists;

