export const MODAL_SHOW = 'MODAL_SHOW';
export const MODAL_HIDE = 'MODAL_HIDE';

export const MODAL_LOGIN     = 'MODAL_LOGION';
export const MODAL_TASK_EDIT = 'MODAL_TASK_EDIT';
export const MODAL_TEXT      = 'MODAL_TEXT';
export const MODAL_TASK_INFO = 'MODAL_TASK_INFO';

export const INPUT_FOCUS = 'INPUT_FOCUS';