// API action types
export const FETCH_USER      = 'FETCH_USER';
export const GOT_USER        = 'GOT_USER';
export const GOT_USER_FAILED = 'GOT_USER_FAILED';

export const FETCH_LISTS      = 'FETCH_LISTS';
export const GOT_LISTS        = 'GOT_LISTS';
export const GOT_LISTS_FAILED = 'GOT_LISTS_FAILED';

// export const FETCH_ALL_LIST      = 'FETCH_ALL_LIST';
// export const GOT_ALL_LIST        = 'GOT_ALL_LIST';
// export const GOT_ALL_LIST_FAILED = 'GOT_ALL_LIST_FAILED';


export const ADD_LIST_REQUEST = 'ADD_LIST_REQUEST';
export const ADD_LIST         = 'ADD_LIST';
export const ADD_LIST_FAILED  = 'ADD_LIST_FAILED';

export const UPDATE_LIST_REQUEST = 'UPDATE_LIST_REQUEST';
export const UPDATE_LIST         = 'UPDATE_LIST';
export const UPDATE_LIST_FAILED  = 'UPDATE_LIST_FAILED';

export const ADD_TASK_REQUEST = 'ADD_TASK_REQUEST';
export const ADD_TASK         = 'ADD_TASK';
export const ADD_TASK_FAILED  = 'ADD_TASK_FAILED';

export const UPDATE_TASK_INDEX_REQUEST = 'UPDATE_TASK_INDEX_REQUEST';
export const UPDATE_TASK_INDEX         = 'UPDATE_TASK_INDEX';
export const UPDATE_TASK_INDEX_FAILED  = 'UPDATE_TASK_INDEX_FAILED';

export const UPDATE_TASK_REQUEST = 'UPDATE_TASK_REQUEST';
export const UPDATE_TASK         = 'UPDATE_TASK';
export const UPDATE_TASK_FAILED  = 'UPDATE_TASK_FAILED';

export const DELETE_TASK_REQUEST = 'DELETE_TASK_REQUEST';
export const DELETE_TASK         = 'DELETE_TASK';
export const DELETE_TASK_FAILED  = 'DELETE_TASK_FAILED';


// UI action types
export const UPDATE_UI = 'UPDATE_UI';


// User 
export const USER_LOGIN         = 'USER_LOGIN';
export const USER_LOGIN_FAILED  = 'USER_LOGIN_FAILED';
export const USER_LOGOUT        = 'USER_LOGOUT';
export const USER_LOGOUT_FAILED = 'USER_LOGOUT_FAILED';