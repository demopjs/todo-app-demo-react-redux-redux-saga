import * as types from 'constants/general-action-types';
import * as uiTypes from 'constants/ui-types';

const INITIAL_STATE = {
  listingOffset: 0,
  // modal
  isModalShown: false,
  modalType: 'MODAL_TASK_INFO',
  modalOptions: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case types.UPDATE_UI :
      if(action.data) {
        return { ...state, ...action.data };
      } 
      return state;


    case uiTypes.MODAL_SHOW :
      if(action.modalType && action.modalOptions) {
        return {
          ...state,
          isModalShown : true,
          modalType    : action.modalType,
          modalOptions : action.modalOptions,
        };
      }
      return state;

    // case /_FAILED/.test(action.type) :
    // case String(action.type.match(/.*_FAILED$/)) :
    case (action.type.match(/_FAILED/) || {}).input :
      let text = null;
      if(action.e && action.e.response && action.e.response.data && 
        action.e.response.data.message) {
        text = `Ошибка. ${action.e.response.data.message}`;
      } else {
        text = 'Нельзя выполнить действие. Неизвестная ошибка.';
      }
      return {
        isModalShown : true,
        modalType    : uiTypes.MODAL_TEXT,
        modalOptions : { text },
      };
      

    case uiTypes.MODAL_HIDE :
      return {
        ...state,
        isModalShown : false,
        modalType    : '',
        modalOptions : {},
      };
      

    default :
      return state;
  }
};
