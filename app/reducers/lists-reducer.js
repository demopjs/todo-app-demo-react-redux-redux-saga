import * as types from 'constants/general-action-types';

import findIndex from 'lodash/findIndex';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    // case types.FETCH_ALL_LIST :
    //   if (action.payload) {
    //     return [ ...action.payload ];
    //   }
    //   return state;


    case types.GOT_LISTS :
      if (action.payload.data) {
        return action.payload.data;
      }
      return state;


    case types.ADD_LIST :
      if (action.payload.data) {
        return [ 
          ...state.map((i) => ({ ...i, isActive: false })), 
          { ...action.payload.data, isActive: true },
        ];
      }
      return state;


    case types.UPDATE_LIST_REQUEST :
      if(action.data && action.data.id) {
        
        return state.map((i) => {
          if(i.id === action.data.id) {
            return {
              ...i,
              ...action.data,
            };
          }
          return i;
        });

      }
      return state;


    case types.UPDATE_LIST :
      if(action.payload.data) {
        
        return state.map((i) => {
          if(i.id === action.payload.data.id) {
            return {
              ...i,
              ...action.payload.data,
            };
          }
          return i;
        });
      }
      return state;


    case types.ADD_TASK :
      if(action.payload.data && action.listId) {
        return state.map((list) => {
          if(list.id === action.listId) {
            return {
              ...list,
              tasks: [
                action.payload.data,
                ...list.tasks,
              ],
            };
          }
          return list;
        });
      }
      return state;


    case types.UPDATE_TASK_REQUEST :
      if(action.id && action.data && action.data.id) {
        return state.map((list) => {
          if(list.id === action.id) {
            const updatedTasks = list.tasks.map((task) => {
              if(task.id === action.data.id) {
                return {
                  ...task,
                  ...action.data,
                };
              }
              return task;
            });
            return {
              ...list,
              tasks: updatedTasks,
            };
          }
          return list;
        });
      }
      return state;



    case types.UPDATE_TASK_INDEX :
      if(action.listId && action.payload.data && action.payload.data.id) {
        return state.map((list) => {
          if(list.id === action.listId) {
            const updatedTasks = list.tasks.map((task) => {
              if(task.id === action.payload.data.id) {
                return {
                  ...task,
                  ...action.payload.data,
                };
              }
              return task;
            });
            return {
              ...list,
              tasks: updatedTasks,
            };
          }
          return list;
        });
      }
      return state;      


    // case types.UPDATE_TASK :
    //   if(action.listId && action.payload.data && action.payload.data.id) {
    //     return state.map((list) => {
    //       if(list.id === action.listId) {
    //         const updatedTasks = list.tasks.map((task) => {
    //           if(task.id === action.payload.data.id) {
    //             return {
    //               ...task,
    //               ...action.payload.data,
    //             };
    //           }
    //           return task;
    //         });
    //         return {
    //           ...list,
    //           tasks: updatedTasks,
    //         };
    //       }
    //       return list;
    //     });
    //   }
    //   return state;


    case types.DELETE_TASK :
      if(action.listId && action.id) {
        const listIndex = findIndex(state, (i) => (i.id === action.listId));
        if(listIndex >= 0) {
          const tasks = state[listIndex].tasks;
          const taskIndex = findIndex(tasks, (i) => (i.id === action.id));
          if(taskIndex >= 0) {
            return [
              ...state.slice(0, listIndex),
              {
                ...state[listIndex],
                tasks : [
                  ...tasks.slice(0, taskIndex),
                  ...tasks.slice(taskIndex + 1),
                ],
              },
              ...state.slice(listIndex + 1),
            ];
          }
        }
      }
      return state;

    default :
      return state;
  }

  return state;
};
