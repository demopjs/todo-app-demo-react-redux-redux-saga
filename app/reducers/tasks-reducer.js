import * as types from 'constants/general-action-types';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case types.INITIAL_FETCH_DATA :
      if (action.payload) {
        const data = action.payload;
        data[0].isActive = true;
        return [ ...data ];
      }
      return state;

    default :
      return state;
  }
};
