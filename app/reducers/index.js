import { combineReducers } from 'redux';

import lists from 'reducers/lists-reducer';
import user from 'reducers/user-reducer';
import ui from 'reducers/ui-reducer';
// import tasks from 'reducers/tasks-reducer';

const rootReducer = combineReducers({
  lists, user, ui
});

export default rootReducer;
