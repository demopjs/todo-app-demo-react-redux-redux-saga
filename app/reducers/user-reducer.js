import * as types from 'constants/general-action-types';

const INITIAL_STATE = {
  logined    : false,
  username   : null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {

    case types.GOT_USER :
      if(action.payload.data) {
        return { ...state, ...action.payload.data };
      } 
      return state;


    // case types.USER_LOGIN :
    //   if(action.payload) {
    //     return { ...state, ...action.payload, isLoggedIn: true };
    //   } 
    //   return state;

    // case types.USER_LOGOUT :
    //   return INITIAL_STATE;


    default :
      return state;
  }
};