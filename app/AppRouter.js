import React, { Component } from 'react';
import { Router, Route, browserHistory } from 'react-router';

import * as routTypes from 'constants/route-types';

// views
import App from 'containers/App/App';
import NoMatch from 'containers/NoMatch/NoMatch';


class AppRouter extends Component {

  render() {
    return (
      <Router history={browserHistory}>
        <Route path="/" component={App} />

        <Route path="/list/:id" routeType={routTypes.ROUTE_DEFAULT_LIST} component={App} />

        <Route path="/new-list" routeType={routTypes.ROUTE_NEW_LIST} component={App} />

        <Route path="*" component={NoMatch} />
      </Router>
    );
  }

}

export default AppRouter;