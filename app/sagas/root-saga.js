import { takeLatest, takeEvery, throttle } from 'redux-saga';
import { call, put } from 'redux-saga/effects';

import * as api from 'modules/api';

import * as types from 'constants/general-action-types';

import { browserHistory } from 'react-router';

const delay = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

export function* fetchUser() {
  try {
    const payload = yield call(api.fetchUser);
    yield put({ type: types.GOT_USER, payload });
  } catch(e) {
    yield put({ type: types.GOT_USER_FAILED, e });
  }
}


export function* fetchLists(action) {
  try {
    const payload = yield call(api.fetchLists, action.id || false);
    yield put({ type: types.GOT_LISTS, payload });
  } catch(e) {
    yield put({ type: types.GOT_LISTS_FAILED, e });
  }
}

export function* addList(action) {
  try {
    const payload = yield call(api.newList);
    yield put({ type: types.ADD_LIST, payload });
    browserHistory.push(`/list/${payload.data.id}`);
    if(action.data && action.data.shouldFetchListsFirst) {
      const listsPayload = yield call(api.fetchLists);
      yield put({ type: types.GOT_LISTS, payload: listsPayload });
    }
  } catch(e) {
    yield put({ type: types.ADD_LIST_FAILED, e });
  }
}

export function* updateList(action) {
  try {
    yield call(delay, 500);
    const payload = yield call(api.updateList, action.data.id, action.data);
    yield put({ type: types.UPDATE_LIST, payload });
  } catch(e) {
    yield put({ type: types.UPDATE_LIST_FAILED, e });
  }
}

export function* addTask(action) {
  try {
    const payload = yield call(api.newTask, action.listId, action.data);
    yield put({ type: types.ADD_TASK, listId: action.listId, payload });
  } catch(e) {
    yield put({ type: types.ADD_TASK_FAILED, e });
  }
}

export function* updateTask(action) {
  try {
    yield call(delay, 500);
    const payload = yield call(api.updateTask, action.data.id, action.data);
    yield put({ type: types.UPDATE_TASK, listId: action.id, payload });
  } catch(e) {
    yield put({ type: types.UPDATE_TASK_FAILED, e });
  }
}

export function* updateTaskIndex(action) {
  try {
    const payload = yield call(api.updateTask, action.data.id, action.data);
    yield put({ type: types.UPDATE_TASK_INDEX, listId: action.id, payload });
  } catch(e) {
    yield put({ type: types.UPDATE_TASK_INDEX_FAILED, e });
  }
}

export function* deleteTask(action) {
  try {
    const payload = yield call(api.deleteTask, action.id);
    yield put({ type: types.DELETE_TASK, listId: action.listId, id: action.id });
  } catch(e) {
    yield put({ type: types.UPDATE_TASK_FAILED, e });
  }
}


export function* rootSaga() {
  yield [
    // FETCH USER
    takeEvery(types.FETCH_USER, fetchUser),
    // FETCH LISTS
    takeLatest(types.FETCH_LISTS, fetchLists),
    // ADD LIST
    takeEvery(types.ADD_LIST_REQUEST, addList),
    // UPDATE LIST
    takeLatest(types.UPDATE_LIST_REQUEST, updateList),
    // ADD TASK
    takeEvery(types.ADD_TASK_REQUEST, addTask),
    // UPDATE TASK
    takeLatest(types.UPDATE_TASK_REQUEST, updateTask),
    takeEvery(types.UPDATE_TASK_INDEX_REQUEST, updateTaskIndex),
    // DELETE TASK
    takeEvery(types.DELETE_TASK_REQUEST, deleteTask),
  ];
}