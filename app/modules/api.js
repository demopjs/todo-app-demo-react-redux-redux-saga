import axios from 'axios';

const API_URL = '/api/';

export const fetchUser = () => {
  return axios.get(`${API_URL}user`);
};

export const fetchLists = (id) => {
  return axios.get(`${API_URL}lists/${id || ''}`);
};

export const newList = () => {
  return axios.post(`${API_URL}new_list`);
};

export const updateList = (listId, data = {}) => {
  if(!listId) {
    console.error('Requested for a new task, but listId was not specified.');
    return false;
  }
  return axios.post(`${API_URL}list/${listId}`, data);
};


export const newTask = (listId, data = {}) => {
  if(!listId) {
    console.error('Requested for a new task, but listId was not specified.');
    return false;
  }
  return axios.post(`${API_URL}new_task/${listId}`, data);
};

export const deleteTask = (taskId) => {
  if(!taskId) {
    console.error('Requested for a new task, but taskId was not specified.');
    return false;
  }
  return axios.post(`${API_URL}delete_task/${taskId}`);
};

export const updateTask = (taskId, data = {}) => {
  if(!taskId) {
    console.error('Requested for a new task, but taskId was not specified.');
    return false;
  }
  return axios.post(`${API_URL}task/${taskId}`, data);
};