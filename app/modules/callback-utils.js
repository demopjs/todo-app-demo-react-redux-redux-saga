export const encodeCallback = (object) => {
  let json, encoded;
  try {
    json = JSON.stringify(object);
  } catch(err) {
    console.warn('Error JSON.stringify on callback`s object');
    return false;
  }

  try {
    encoded = encodeURI(json);
  } catch(err) {
    console.warn('Error encodeURI on callback`s json string');
    return false; 
  }

  return encoded;
}


export const decodeCallback = (string) => {
  let object, decoded;
  try {
    decoded = decodeURI(string);
  } catch(err) {
    console.warn('Error decodeURI on callback`s string');
    return false; 
  }

  try {
    object = JSON.parse(decoded);
  } catch(err) {
    console.warn('Error JSON.parse on callback`s string');
    return false;
  }

  return object;
}